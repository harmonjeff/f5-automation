variable "bigip1_private_ip" {
   type = list
   default = ["10.2.1.50","10.2.1.51","10.2.1.52","10.2.1.53","10.2.1.54"]
}

variable "bigip2_private_ip" {
   type = list
   default = ["10.2.2.60","10.2.2.61","10.2.2.62","10.2.2.63","10.2.2.64"]
}

variable "bigip1_default_route" {
   type = string
   default = "10.2.1.1"
}
variable "bigip2_default_route" {
   type = string
   default = "10.2.2.1"
}

variable "bigip1_eip_id" {
   type = string
   default = "eipalloc-0eb441694dcbb8896"
}

variable "bigip1_mgmt_ip" {
   type = string
   default = "52.37.242.114"
}

variable "bigip2_eip_id" {
   type = string
   default = "eipalloc-00b75a9287c6ddca5"
}

variable "bigip2_mgmt_ip" {
   type = string
   default = "35.85.95.0"
}

variable "juice_shop_eip_id" {
   type = string
   default = "eipalloc-061dd82224981b122"
}

variable "restricted_ip_address" {
   type = string
}

variable "cloud_failover_label" {
   type = string
   default = "ewsseclabdemo"
}
