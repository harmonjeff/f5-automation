---
# Install the various iLX extensions on the BigIPs.  
# This demo uses the DO, CFE, and AS3 extensions, but the TS extension is installed since it is best practice from F5
# and the information from TS can be helpful for operational purposes (sizing)
- hosts: bigips
  gather_facts: false
  vars:
    # temporary place to put state files since many of these tasks are not idempotent
    generated_dir: "{{ playbook_dir | dirname }}/generated"
    # password to set and use for the admin account on the BigIP - this should be changed to use secrets management for production usage
    bigip_admin_password: "{{ lookup('env', 'bigip_admin_password') }}"
  roles:
    # wait for the AWS EC2 instances running BigIP to become available
    - aws_wait_for_bigip_instance
    
    # set the password and default shell for the admin account to a known value
    - ssh_set_bigip_admin_password

    # Add private CA root certificate to device trust
    - ssh_add_private_ca_device_trust

    # All of these extensions are the most current versions as of 9/29/2021

    # install the Declarative Onboarding (do) extension
    # find latest release at https://github.com/F5Networks/f5-declarative-onboarding/releases
    - role: f5_install_extension
      major_version: 1.24.0
      minor_version: 6
      extension_uri_name: declarative-onboarding
      rpm_url: "https://github.com/F5Networks/f5-declarative-onboarding/releases/download/v{{ major_version }}/f5-declarative-onboarding-{{ major_version }}-{{ minor_version }}.noarch.rpm"
      rpm_name: "f5-declarative-onboarding-{{ major_version }}-{{ minor_version }}.noarch.rpm"
      rpm_short_name: "do"
    # install the Applicaiton Services 3 (as3) extension
    # find latest release at https://github.com/F5Networks/f5-appsvcs-extension/releases
    - role: f5_install_extension
      major_version: 3.31.0
      minor_version: 6
      extension_uri_name: appsvcs
      rpm_url: "https://github.com/F5Networks/f5-appsvcs-extension/releases/download/v{{ major_version }}/f5-appsvcs-{{ major_version }}-{{ minor_version }}.noarch.rpm"
      rpm_name: "f5-appsvcs-{{ major_version }}-{{ minor_version }}.noarch.rpm"
      rpm_short_name: "as3"
    # install the Telemetry Streaming (ts) extension
    # find latest release at https://github.com/F5Networks/f5-telemetry-streaming/releases
    - role: f5_install_extension
      major_version: 1.22.0
      minor_version: 1
      extension_uri_name: telemetry-streaming
      rpm_url: "https://github.com/F5Networks/f5-telemetry-streaming/releases/download/v{{ major_version }}/f5-telemetry-{{ major_version }}-{{ minor_version }}.noarch.rpm"
      rpm_name: "f5-telemetry-{{ major_version }}-{{ minor_version }}.noarch.rpm"
      rpm_short_name: "ts"
    # install the Cloud Failover (cfe) extension
    # find latest release at https://github.com/f5networks/f5-cloud-failover-extension/releases
    - role: f5_install_extension
      major_version: 1.9.0
      minor_version: 0
      extension_uri_name: cloud-failover
      rpm_url: "https://github.com/F5Networks/f5-cloud-failover-extension/releases/download/v{{ major_version }}/f5-cloud-failover-{{ major_version }}-{{ minor_version }}.noarch.rpm"
      rpm_name: "f5-cloud-failover-{{ major_version }}-{{ minor_version }}.noarch.rpm"
      rpm_short_name: "cfe"
