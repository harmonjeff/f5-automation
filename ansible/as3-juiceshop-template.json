{
    "$schema": "https://raw.githubusercontent.com/F5Networks/f5-appsvcs-extension/master/schema/latest/as3-schema.json",
    "class": "ADC",
    "schemaVersion": "3.31.0",
    "Common": {
        "class": "Tenant",
        "Shared": {
            "class": "Application",
            "template": "shared",
            "juice-shop-pool": {
                "class": "Pool",
                "monitors": [
                    {
                        "bigip": "/Common/http"
                    }
                ],
                "members": [
                    {
                        "servicePort": 3000,
                        "addressDiscovery": "aws",
                        "region": "us-west-2",
                        "updateInterval": 20,
                        "tagKey": "findme",
                        "tagValue": "web",
                        "addressRealm": "private",
                        "adminState": "enable",
                        "autoPopulate": true,
                        "enable": true
                    }
                ]
            },
            "private_ca": {
                "class": "CA_Bundle",
                "bundle": "{{ private_ca }}"
            }
        }
    },
    "JuiceShop": {
        "class": "Tenant",
        "Shared": {
            "class": "Application",
            "template": "shared",
            "clientssl_secure_juiceshop": {
                "certificates": [
                    {
                        "certificate": "/JuiceShop/Shared/certificate_juiceshop"
                    }
                ],
                "ciphers": "ECDHE-RSA-AES256-GCM-SHA384",
                "tls1_0Enabled": false,
                "tls1_1Enabled": false,
                "tls1_2Enabled": true,
                "tls1_3Enabled": false,
                "singleUseDhEnabled": true,
                "renegotiationEnabled": false,
                "class": "TLS_Server"
            },
            "certificate_juiceshop": {
                "class": "Certificate",
                "remark": "in practice we recommend using a passphrase",
                "pkcs12Options": {
                    "keyImportFormat": "pkcs8"
                },
                "certificate": "{{ juiceshop_cert }}",
                "privateKey": "{{ juiceshop_key }}",
                "passphrase": {
                    "ciphertext": "{{ juiceshop_key_password }}",
                    "protected": "eyJhbGciOiJkaXIiLCJlbmMiOiJub25lIn0"
                },
                "chainCA": {
                    "use": "/Common/Shared/private_ca"
                }
            },
            "http2_juiceshop": {
                "class": "HTTP2_Profile",
                "enforceTlsRequirements": false
            },
            "juiceshop-a": {
                "remark": "basic standard virtual server",
                "layer4": "tcp",
                "pool": "/Common/Shared/juice-shop-pool",
                "translateServerAddress": true,
                "translateServerPort": true,
                "class": "Service_HTTPS",
                "profileHTTP": {
                    "bigip": "/Common/http"
                },
                "profileMultiplex": {
                    "bigip": "/Common/oneconnect"
                },
                "profileTCP": {
                    "bigip": "/Common/tcp"
                },
                "serverTLS": "clientssl_secure_juiceshop",
                "virtualAddresses": [
                    ["{{ juiceshop_a_ip }}","{{ restricted_ip_address }}/32"]
                ],
                "virtualPort": 443,
                "snat": "auto",
                "securityLogProfiles": [{
                    "use": "juice_shop_log"
                }],
                "profileDOS": {
                    "use": "juiceshop_dos_profile"
                },
                "policyWAF": {
                    "use": "juice_shop_waf"
                }
            },
           "juiceshop-b": {
                "remark": "basic standard virtual server",
                "layer4": "tcp",
                "pool": "/Common/Shared/juice-shop-pool",
                "translateServerAddress": true,
                "translateServerPort": true,
                "class": "Service_HTTPS",
                "profileHTTP": {
                    "bigip": "/Common/http"
                },
                "profileMultiplex": {
                    "bigip": "/Common/oneconnect"
                },
                "profileTCP": {
                    "bigip": "/Common/tcp"
                },
                "serverTLS": "clientssl_secure_juiceshop",
                "virtualAddresses": [
                    ["{{ juiceshop_b_ip }}","{{ restricted_ip_address }}/32"]
                ],
                "virtualPort": 443,
                "snat": "auto",
                "securityLogProfiles": [{
                    "use": "juice_shop_log"
                    }],
                    "profileDOS": {
                        "use": "juiceshop_dos_profile"
                    },
                    "policyWAF": {
                        "use": "juice_shop_waf"
                    }
                },
            "juice_shop_log": {
                "class": "Security_Log_Profile",
                "remark": "EWS Seclab Demo Logging",
                "application": {
                    "localStorage": true,
                    "storageFilter": {
                        "requestType": "all"
                    }
                },
                "botDefense": {
                    "localPublisher":{
                        "bigip": "/Common/local-db-publisher"
                    },
                    "logUntrustedBot": true,
                    "logSuspiciousBrowser": true,
                    "logMaliciousBot": true,
                    "logUnknown": true,
                    "logAlarm": true,
                    "logCaptcha": true,
                    "logRateLimit": true,
                    "logBlock": true,
                    "logTcpReset": true,
                    "logHoneyPotPage": true,
                    "logRedirectToPool": true,
                    "logBotSignatureMatchedRequests": true,
                    "logChallengeFailureRequest": true,
                    "logIllegalRequests": true
                },
                "dosApplication": {
                    "localPublisher": {
                        "bigip": "/Common/local-db-publisher"
                    }
                }
            },
            "juiceshop_dos_profile": {
                "class": "DOS_Profile",
                "remark": "DOS profile protecting the Juice Shop Application",
                "application": {
                    "stressBasedDetection": {
                        "operationMode": "blocking",
                        "thresholdsMode": "manual",
                        "sourceIP": {
                            "captchaChallengeEnabled": true,
                            "clientSideDefenseEnabled": true,
                            "tpsIncreaseRate": 500,
                            "minimumTps": 40,
                            "maximumTps": 200
                        },
                        "deviceID": {
                            "captchaChallengeEnabled": true,
                            "clientSideDefenseEnabled": true,
                            "tpsIncreaseRate": 500,
                            "minimumTps": 40,
                            "maximumTps": 200
                        },
                        "badActor": {
                            "detectionEnabled": true,
                            "mitigationMode": "aggressive",
                            "signatureDetectionEnabled": true,
                            "tlsSignaturesEnabled": true
                        },
                        "escalationPeriod": 120,
                        "deEscalationPeriod": 7200
                    },
                    "botDefense": {
                        "blockSuspiscousBrowsers": true,
                        "issueCaptchaChallenge": true,
                        "mode": "during-attacks"
                    }
                }
            },
            "juice_shop_waf": {
                "class": "WAF_Policy",
                "enforcementMode": "blocking",
                "ignoreChanges": false,
                "serverTechnologies": [
                    "AngularJS",
                    "Node.js",
                    "SQLite",
                    "Unix/Linux"
                ],
                "url": {
                    "skipCertificateCheck": true,
                    "url": "https://gitlab.com/harmonjeff/juice-shop-asm-waf/raw/main/juice_shop_waf.json"
                }
            }
        }
    }
}