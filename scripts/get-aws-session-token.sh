#!/bin/bash
if [ $# -eq 0 ]
  then
    echo "Must provide the current MFA token as a paramter"
    exit 1
fi

if [ -z $AWS_ACCOUNT_ID ]
then
  echo "Must define AWS_ACCOUNT_ID environment variable"
  exit 1
fi

if [ -z $AWS_IAM_USER ]
then
  echo "Must define AWS_IAM_USER environment variable"
  exit 1
fi

aws sts get-session-token --output json --serial-number arn:aws:iam::$AWS_ACCOUNT_ID:mfa/$AWS_IAM_USER --token-code $1 > aws_session_token.json

AWS_NEW_ACCESS_KEY_ID=$(cat aws_session_token.json | jq -r '. | .Credentials.AccessKeyId')
AWS_NEW_SECRET_ACCESS_KEY=$(cat aws_session_token.json | jq -r '. | .Credentials.SecretAccessKey')
export AWS_SESSION_TOKEN=$(cat aws_session_token.json | jq -r '. | .Credentials.SessionToken')

if [ -z $AWS_NEW_ACCESS_KEY_ID ]
then
  FAILURE_MESSAGE=$(cat aws_session_token.json)
  echo "aws sts get-session-token failed $FAILURE_MESSAGE"
  exit 1
fi

if [ -z $AWS_NEW_SECRET_ACCESS_KEY ]
then
  FAILURE_MESSAGE=$(cat aws_session_token.json)
  echo "aws sts get-session-token failed $FAILURE_MESSAGE"
  exit 1
fi

if [ -z $AWS_SESSION_TOKEN ]
then
  FAILURE_MESSAGE=$(cat aws_session_token.json)
  echo "aws sts get-session-token failed $FAILURE_MESSAGE"
  exit 1
fi

export AWS_ACCESS_KEY_ID=$AWS_NEW_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$AWS_NEW_SECRET_ACCESS_KEY

echo "export AWS_ACCESS_KEY_ID=$AWS_NEW_ACCESS_KEY_ID" > .aws-creds
echo "export AWS_SECRET_ACCESS_KEY=$AWS_NEW_SECRET_ACCESS_KEY" >> .aws-creds
echo "export AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN" >> .aws-creds
#Set environment variables that Terraform will use to connect to AWS
echo "export TF_VAR_aws_access_key_id=$AWS_NEW_ACCESS_KEY_ID" >> .aws-creds
echo "export TF_VAR_aws_secret_access_key=$AWS_NEW_SECRET_ACCESS_KEY" >> .aws-creds
echo "export TF_VAR_aws_session_token=$AWS_SESSION_TOKEN" >> .aws-creds

echo "Successfully created .aws-creds, source it if on Mac"
