#!/bin/bash

# NOTE: ECDSA certs can't be used in conjunction with the cipher suites limited to ECDHE-RSA-AES256-GCM-SHA384
# Certificates using RSA keys must be used instead.

if [ ! -d certs ]
then
mkdir certs
fi

if [ ! -f certs/private_ca.key ]
then
# For the purposes of this demo, generate the private key of the "private" (internal) CA
openssl ecparam -out certs/private_ca.key 2048
fi

if [ ! -f certs/private_ca.full.cert ]
then
# Generate the root certificate of the private CA using the private key
openssl req -x509 -new -nodes -key certs/private_ca.key -sha384 -days 3650 \
  -out certs/private_ca.full.cert \
  -subj "/C=US/ST=EWS/L=EWSSeclab/O=Early\ Warning/CN=Early\ Warning\ Mock\ CA"
fi

# Generate the private key for the juice shop server certificate
openssl ecparam -out certs/juiceshop.ewsseclab.dev.full.key -name prime256v1 -genkey

# Generate the CSR for the juice shop server certificate so that the private CA can sign it
openssl req -new -nodes -key certs/juiceshop.ewsseclab.dev.full.key -out certs/juiceshop.ewsseclab.dev.csr \
  -subj "/C=US/ST=EWS/L=EWSSecLab/O=Early\ Warning/CN=juiceshop.ewsseclab.dev"

# Generate the juice shop certificate from the CSR and sign it with the private CA
openssl x509 -req -in certs/juiceshop.ewsseclab.dev.csr -sha384 -days 1095 \
  -CA certs/private_ca.full.cert -CAkey certs/private_ca.key -CAcreateserial \
  -out certs/juiceshop.ewsseclab.dev.full.cert

#openssl ecparam -out example01b.tmp.key -name prime256v1 -genkey
#openssl req -new -days 1 -nodes -x509 \
#    -subj "/C=US/ST=Denial/L=F5Automation/O=Dis/CN=bigip1.ewsseclab.dev" \
#    -key example01b.tmp.key -out example01b.tmp.cert

awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' certs/juiceshop.ewsseclab.dev.full.key > certs/juiceshop.ewsseclab.dev.key
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' certs/juiceshop.ewsseclab.dev.full.cert > certs/juiceshop.ewsseclab.dev.cert

#sed -i .tmp 's/\\n/\\\\n/g' certs/juiceshop.ewsseclab.dev.key
#rm certs/juiceshop.ewsseclab.dev.key.tmp

if [ $? -eq 0 ]
then
  echo "The script ran ok"
else
  echo "The script failed" >&2
fi

