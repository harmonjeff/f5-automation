#!/bin/bash

# NOTE: Could never make this work. F5 DO doesn't support private keys protected by a passphrase
# and AS3 failed with this message when setting a passphrase:
# {"message":"Declaration failed: 01070712:3: Unable to verify key (/JuiceShop/Shared/certificate_juiceshop.key) is protected by provided passphrase.","level":"error"}
# or this message when the private key was protected by a passphrase and the AS3 JSON didn't have it:
# {"message":"Declaration failed: 01070712:3: Certificate/Key has unknown format or security type (/JuiceShop/Shared/certificate_juiceshop.key).","level":"error"}

#export private_ca_password="f5f5"

if [ ! -d certs ]
then
mkdir certs
fi

if [ ! -f certs/private_ca.key ]
then
# For the purposes of this demo, generate the private key of the "private" (internal) CA
openssl genrsa -aes256 -out certs/private_ca.key -passout pass:$private_ca_password 2048
fi

if [ ! -f certs/private_ca.full.cert ]
then
# Generate the root certificate of the private CA using the private key
openssl req -x509 -new -nodes -key certs/private_ca.key -passin pass:$private_ca_password -sha384 -days 3650 \
  -out certs/private_ca.full.cert \
  -subj "/C=US/ST=EWS/L=EWSSeclab/O=Early\ Warning/CN=Early\ Warning\ Mock\ CA"
fi

# Generate the private key of the wildcard server certificate that will be used with the management UI and Juice Shop application
# NOTE: The AS3 schema allows for a passphrase for a private key to be passed, so encrypt the private key with AES 256 and set a
# passphrase
openssl genrsa -aes256 -out certs/ewsseclab.dev.full.key -passout pass:$private_ca_password 2048

# Generate a CSR for the wildcard server certificate
openssl req -new -nodes -key certs/ewsseclab.dev.full.key -passin pass:$private_ca_password -out certs/ewsseclab.dev.csr \
  -subj "/C=US/ST=EWS/L=EWSSecLab/O=Early\ Warning/CN=*.ewsseclab.dev"

# Generate the wildcard server certificate and sign it with the private CA
openssl x509 -req -in certs/ewsseclab.dev.csr -sha384 -days 1095 \
  -CA certs/private_ca.full.cert -CAkey certs/private_ca.key -passin pass:$private_ca_password -CAcreateserial \
  -out certs/ewsseclab.dev.full.cert

# Generate the private key for the bigip1 certificate
# NOTE: As of version 1.24 of the F5 DO extension, there is no way to use a passphrase with the private key for a device certificate
# so don't encrypt the private key with -aes256
openssl genrsa -out certs/bigip1.ewsseclab.dev.full.key -passout pass:$private_ca_password 2048

# Generate a CSR for bigip1
openssl req -new -nodes -key certs/bigip1.ewsseclab.dev.full.key -passin pass:$private_ca_password -out certs/bigip1.ewsseclab.dev.csr \
  -subj "/C=US/ST=EWS/L=EWSSecLab/O=Early\ Warning/CN=bigip1.ewsseclab.dev"

# Generate and sign the certificate for bigip1
openssl x509 -req -in certs/bigip1.ewsseclab.dev.csr -sha384 -days 1095 \
  -CA certs/private_ca.full.cert -CAkey certs/private_ca.key -passin pass:$private_ca_password -CAcreateserial \
  -out certs/bigip1.ewsseclab.dev.full.cert

# Generate the private key for the bigip2 certificate
openssl genrsa -out certs/bigip2.ewsseclab.dev.full.key -passout pass:$private_ca_password 2048

# Generate a CSR for bigip2
openssl req -new -nodes -key certs/bigip2.ewsseclab.dev.full.key -passin pass:$private_ca_password -out certs/bigip2.ewsseclab.dev.csr \
  -subj "/C=US/ST=EWS/L=EWSSecLab/O=Early\ Warning/CN=bigip2.ewsseclab.dev"

# Generate and sign the certificate for bigip1
openssl x509 -req -in certs/bigip2.ewsseclab.dev.csr -sha256 -days 1095 \
  -CA certs/private_ca.full.cert -CAkey certs/private_ca.key -passin pass:$private_ca_password -CAcreateserial \
  -out certs/bigip2.ewsseclab.dev.full.cert

# Create the single line files of the certificates and keys needed for DO JSON files
# This was used in the F5 labs, but the jq method below works with the key file when it is
# protected by a passphrase
#awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' certs/private_ca.full.cert > certs/private_ca.cert
#awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' certs/ewsseclab.dev.full.key > certs/ewsseclab.dev.key
#awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' certs/ewsseclab.dev.full.cert > certs/ewsseclab.dev.cert

# Command recommonded in F5 documentation that is more reliable for putting the certificate
# in a single line format as needed by AS3. Wish that F5 would just let us base64 encode the
# certificate for use in AS3 like is done in DO. The trailing "tr -d '"'' was added so that
# the file wouldn't have double quotes in it and the AS3 JSON file could be more compliant with
# the AS3 schema.
# https://support.f5.com/csp/article/K14869003
jq -Rs . < certs/private_ca.full.cert | tr -d '"' > certs/private_ca.cert
jq -Rs . < certs/ewsseclab.dev.full.cert | tr -d '"' > certs/ewsseclab.dev.cert
jq -Rs . < certs/ewsseclab.dev.full.key | tr -d '"' > certs/ewsseclab.dev.key


if [ $? -eq 0 ]
then
  echo "The script ran ok"
else
  echo "The script failed" >&2
fi