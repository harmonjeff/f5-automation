#!/bin/bash

if [ -z $1 ]
then
  echo "IP address of the BigIP must be passed as the first parameter"
  exit 1
fi

if [ -z $bigip_admin_password ]
then
  echo "bigip_admin_password environment variable must be set"
  exit 1
fi

FILENAME=generated/bigip-verify-do-$1-response.json

curl --location --request GET 'https://'$1'/mgmt/shared/declarative-onboarding/info' \
--header 'Content-Type: application/json' \
--insecure \
--user 'admin:'$bigip_admin_password \
--data-raw '' \
--silent \
--output $FILENAME

RESULT=$(jq -er '. | .[0].code' $FILENAME)

if [ -z $RESULT ]
then
  echo "curl command to validate the F5 DO iLX package failed"
  exit 1
fi

VERSION="VERSION="$(jq -r '. | .[0].version' $FILENAME)",RELEASE="$(jq -r '. | .[0].release' $FILENAME)

echo "F5 DO iLX is installed: $VERSION"