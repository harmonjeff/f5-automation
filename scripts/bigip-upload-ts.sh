#!/bin/bash

if [ -z $1 ]
then
  echo "IP address of the BigIP must be passed as the first parameter"
  exit 1
fi

if [ -z $bigip_admin_password ]
then
  echo "bigip_admin_password environment variable must be set"
  exit 1
fi

FILENAME=generated/bigip-upload-ts-$1-response.json

curl --location --request POST 'https://'$1'/mgmt/tm/util/bash' \
--header 'Content-Type: application/json' \
--insecure \
--user admin:$bigip_admin_password \
--data-raw '{
	"command": "run", "utilCmdArgs": "-c \"cd /var/config/rest/downloads/ && curl -sk -LJO https://github.com/F5Networks/f5-telemetry-streaming/releases/download/v1.17.0/f5-telemetry-1.17.0-4.noarch.rpm\""
}' \
--silent \
--output $FILENAME

RESULT=`jq -r ". | .command" $FILENAME`

if [ -z $RESULT ]
then
  echo "curl command to upload the F5 TS iLX package failed"
  exit 1
fi

if [ $RESULT != "run" ]
then
  echo "F5 TS iLX package failed to upload.  Check $FILENAME"
  exit 1
fi

echo "F5 TS iLX package was uploaded successfully!"