#!/bin/bash

if [ -z $1 ]
then
  echo "IP address of the BigIP must be passed as the first parameter"
  exit 1
fi

if [ -z $bigip_admin_password ]
then
  echo "bigip_admin_password environment variable must be set"
  exit 1
fi

FILENAME=generated/bigip-install-as3-$1-response.json

curl --location --request POST 'https://'$1'/mgmt/shared/iapp/package-management-tasks' \
--header 'Content-Type: application/json' \
--insecure \
--user 'admin:'$bigip_admin_password \
--data-raw '{
    "operation":"INSTALL","packageFilePath":"/var/config/rest/downloads/f5-appsvcs-3.25.0-3.noarch.rpm"
}' \
--silent \
--output $FILENAME

RESULT=$(jq -r '. | .status' $FILENAME)

if [ -z $RESULT ]
then
  echo "curl command to install the F5 AS3 iLX package failed"
  exit 1
fi

if [ $RESULT == "CREATED" ]
then
  echo "F5 AS3 iLX package was installed successfully!"
else
  echo "F5 AS3 iLX package was not installed successfully. See $FILENAME"
fi