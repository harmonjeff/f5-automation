#!/bin/bash

if [ ! -d certs ]
then
mkdir certs
fi

if [ ! -f certs/private_ca.key ]
then
# For the purposes of this demo, generate the private key of the "private" (internal) CA
openssl genrsa -out certs/private_ca.key 2048
fi

if [ ! -f certs/private_ca.full.cert ]
then
# Generate the root certificate of the private CA using the private key
openssl req -x509 -new -nodes -key certs/private_ca.key -sha384 -days 3650 \
  -out certs/private_ca.full.cert \
  -subj "/C=US/ST=EWS/L=EWSSeclab/O=Early\ Warning/CN=Early\ Warning\ Mock\ CA"
fi

# Generate the private key of the wildcard server certificate that will be used with the management UI and Juice Shop application
openssl genrsa -out certs/ewsseclab.dev.full.key 2048

# Generate a CSR for the wildcard server certificate
openssl req -new -nodes -key certs/ewsseclab.dev.full.key -out certs/ewsseclab.dev.csr \
  -subj "/C=US/ST=EWS/L=EWSSecLab/O=Early\ Warning/CN=*.ewsseclab.dev"

# Generate the wildcard server certificate and sign it with the private CA
openssl x509 -req -in certs/ewsseclab.dev.csr -sha384 -days 1095 \
  -CA certs/private_ca.full.cert -CAkey certs/private_ca.key -CAcreateserial \
  -out certs/ewsseclab.dev.full.cert

# Generate the private key for the bigip1 certificate
openssl genrsa -out certs/bigip1.ewsseclab.dev.full.key 2048

# Generate a CSR for bigip1
openssl req -new -nodes -key certs/bigip1.ewsseclab.dev.full.key -out certs/bigip1.ewsseclab.dev.csr \
  -subj "/C=US/ST=EWS/L=EWSSecLab/O=Early\ Warning/CN=bigip1.ewsseclab.dev"

# Generate and sign the certificate for bigip1
openssl x509 -req -in certs/bigip1.ewsseclab.dev.csr -sha384 -days 1095 \
  -CA certs/private_ca.full.cert -CAkey certs/private_ca.key -CAcreateserial \
  -out certs/bigip1.ewsseclab.dev.full.cert

# Generate the private key for the bigip2 certificate
openssl genrsa -out certs/bigip2.ewsseclab.dev.full.key 2048

# Generate a CSR for bigip2
openssl req -new -nodes -key certs/bigip2.ewsseclab.dev.full.key -out certs/bigip2.ewsseclab.dev.csr \
  -subj "/C=US/ST=EWS/L=EWSSecLab/O=Early\ Warning/CN=bigip2.ewsseclab.dev"

# Generate and sign the certificate for bigip1
openssl x509 -req -in certs/bigip2.ewsseclab.dev.csr -sha256 -days 1095 \
  -CA certs/private_ca.full.cert -CAkey certs/private_ca.key -CAcreateserial \
  -out certs/bigip2.ewsseclab.dev.full.cert

# Create the single line files of the certificates and keys needed for DO JSON files
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' certs/private_ca.full.cert > certs/private_ca.cert
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' certs/ewsseclab.dev.full.key > certs/ewsseclab.dev.key
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' certs/ewsseclab.dev.full.cert > certs/ewsseclab.dev.cert
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' certs/bigip1.ewsseclab.dev.full.key > certs/bigip1.ewsseclab.dev.key
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' certs/bigip1.ewsseclab.dev.full.cert > certs/bigip1.ewsseclab.dev.cert
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' certs/bigip2.ewsseclab.dev.full.key > certs/bigip2.ewsseclab.dev.key
awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' certs/bigip2.ewsseclab.dev.full.cert > certs/bigip2.ewsseclab.dev.cert

if [ $? -eq 0 ]
then
  echo "The script ran ok"
else
  echo "The script failed" >&2
fi