#!/bin/bash
if [ -z $emailid ]
then
    echo "emailid environment variable must be set"
    exit 1
fi

if [ -z $1 ]
then
    echo "The IP of the BigIP to be managed must be passed as first paramter"
    exit 1
fi

ssh -oStrictHostKeyChecking=no -i MyKeyPair-$emailid.pem admin@$1 'modify auth user admin password '$bigip_admin_password'; modify /sys global-settings gui-setup disabled; save /sys config; quit'