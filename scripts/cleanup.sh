#!/bin/bash
rm *tfstate*
rm example*
rm *rendered*
rm *.pem
rm *.svg
rm aws_session_token.json
rm generated/*
rm .aws-creds

if [ $? -eq 0 ]
then
  echo "Success"
else
  echo "The script failed" >&2
fi

