#!/bin/bash

if [ -z $1 ]
then
  echo "IP address of the BigIP must be passed as the first parameter"
  exit 1
fi

if [ -z $2 ]
then
  echo "hostname of the BigIP must be passed as the second parameter"
  exit 1
fi

if [ -z $3 ]
then
  echo "IP of DNS server for the BigIP must be passed as the third parameter"
  exit 1
fi

if [ -z $4 ]
then
  echo "DNS search domain(s) must be passed as the fourth parameter"
  exit 1
fi

if [ -z $5 ]
then
  echo "Self IP (with CIDR like /24) for the BigIP must be passed as the fifth paramter"
  exit 1
fi

if [ -z $6 ]
then
  echo "Default route for BigIP must be passed as the sixth parameter"
  exit 1
fi

if [ -z $bigip_admin_password ]
then
  echo "bigip_admin_password environment variable must be set"
  exit 1
fi

FILENAME=generated/bigip-do-network-modules-$1-response.json

curl --location --request POST 'https://'$1'/mgmt/shared/declarative-onboarding/' \
--header 'Content-Type: application/json' \
--insecure \
--user 'admin:'$bigip_admin_password \
--data-raw '{
    "schemaVersion": "1.0.0",
    "class": "Device",
    "async": true,
    "label": "BIG-IP declaration for declarative onboarding",
    "Common": {
        "class": "Tenant",
        "hostname": "'$2'",
        "myDns": {
            "class": "DNS",
            "nameServers": [
                "'$3'",
                "2001:4860:4860::8844"
            ],
            "search": [
                "'$4'"
            ]
        },
        "myNtp": {
            "class": "NTP",
            "servers": [
                "0.pool.ntp.org",
                "1.pool.ntp.org",
                "2.pool.ntp.org"
            ],
            "timezone": "UTC"
        },
        "myProvisioning": {
            "class": "Provision",
            "ltm": "nominal",
            "avr": "nominal",
            "asm": "nominal"
        },
        "traffic": {
            "class": "VLAN",
            "tag": 4094,
            "mtu": 1500,
            "interfaces": [
                {
                    "name": "1.1",
                    "tagged": false
                }
            ],
            "cmpHash": "src-ip"
        },
        "traffic-self": {
            "class": "SelfIp",
            "address": "'$5'",
            "vlan": "traffic",
            "allowService": "all",
            "trafficGroup": "traffic-group-local-only"
        },
        "Default": {
            "class": "Route",
            "gw": "'$6'",
            "localOnly": true,
            "network": "default",
            "mtu": 1500
        },
        "configsync": {
            "class": "ConfigSync",
            "configsyncIp": "/Common/traffic-self/address"
        },
        "failoverAddress": {
            "class": "FailoverUnicast",
            "address": "/Common/traffic-self/address"
        },
        "dbvars": {
            "class": "DbVariables",
            "ui.advisory.enabled": true,
            "ui.advisory.color": "green",
            "ui.advisory.text": "/Common/hostname"
        }
    }
}' \
--silent \
--output $FILENAME

RESULT=$(jq -er '. | .result.code' $FILENAME)

if [ -z $RESULT ]
then
  echo "curl command to plumb the network and modules of the BigIP failed"
  exit 1
fi

if [ $RESULT == "200"]
then
  echo "DO command was successful!"
  exit
fi

TASKID=$(jq -er '. | .id' $FILENAME)

echo -n "Waiting for DO to finish"

while [ $RESULT == "202" ]
do
  echo -n "."
  sleep 5
  curl --location --request GET 'https://'$1'/mgmt/shared/declarative-onboarding/task/'$TASKID \
  --header 'Content-Type: application/json' \
  --insecure \
  --user 'admin:'$bigip_admin_password \
  --data-raw '' \
  --silent \
  --output $FILENAME
  RESULT=$(jq -er '. | .result.code' $FILENAME)
done

if [ $RESULT != "200" ]
then
  echo "Failed!"
  exit 1
fi

echo "Success!"