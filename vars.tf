variable "web_server_ami" {
  type = map(string)

  default = {
    "us-east-1"      = "ami-a4c7edb2"
    "ap-southeast-1" = "ami-77af2014"
    "us-west-2"      = "ami-6df1e514"
  }
}
variable "aws_region" {
  description = "aws region"
  default     = "us-west-2"
}
variable "bigIqLicenseManager" {
  description = "Management IP address of the BigIQ License Manager"
  default     = "null"
}
variable "bigIqLicensePoolName" {
  description = "BigIQ License Pool name"
  default     = "BigIQLicensePool"
}
variable "server_port" {
  description = "The port the server will use for HTTP requests"
  default     = 3000
}
variable "emailid" {
}
variable "emailidsan" {
}
variable "aws_keypair" {
}
variable "restrictedSrcAddress" {
  type        = list
  description = "Lock down management access by source IP address or network"
  default     = ["0.0.0.0/0"]
}
variable "restrictedSrcAddressVPC" {
  type        = list
  description = "Lock down management access by source IP address or network"
  default     = ["10.2.0.0/16"]
}
variable "bigip_admin" {
  type = string
  default = "admin"
}

variable "bigip_admin_password" {
  type = string
}

variable "aws_access_key_id" {
  type = string
}

variable "aws_secret_access_key" {
  type = string
}

variable "aws_session_token" {
  type = string
} 