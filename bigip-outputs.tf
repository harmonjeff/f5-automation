output "Bigip1InstanceId" {
  value = aws_instance.bigip1.id
}
output "Bigip2InstanceId" {
  value = aws_instance.bigip2.id
}
output "Bigip1ManagementPublicIPAddress" {
  value = aws_instance.bigip1.public_ip
}
output "Bigip2ManagementPublicIPAddress" {
  value = aws_instance.bigip2.public_ip
}
output "bigip1_private_mgmt_address" {
  value = aws_instance.bigip1.private_ip
}
output "bigip2_private_mgmt_address" {
  value = aws_instance.bigip2.private_ip
}
output "bigip1_traffic-self" {
  value = aws_network_interface.bigip1_traffic.private_ips
}
output "bigip2_traffic-self" {
  value = aws_network_interface.bigip2_traffic.private_ips
}
output "f5_ami" {
  value = data.aws_ami.f5.id
}
# output "virtual_server01_elastic_ip" {
#   value = aws_eip.virtual_server01.public_ip
# }
output "bigip1_web_console" {
  value = "https://${aws_instance.bigip1.public_ip}"
}
output "bigip2_web_console" {
  value = "https://${aws_instance.bigip2.public_ip}"
}
