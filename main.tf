provider "aws" {
  #access_key = var.aws_access_key_id
  #secret_key = var.aws_secret_access_key
  #token = var.aws_session_token
  region = var.aws_region
}

resource "aws_vpc" "f5-automation-vpc" {
  cidr_block           = "10.2.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"

  tags = {
    Name = "F5 BigIP Automation Test - ${var.emailid}"
  }
}

resource "aws_subnet" "f5-management-a" {
  vpc_id                  = aws_vpc.f5-automation-vpc.id
  cidr_block              = "10.2.101.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "${var.aws_region}a"

  tags = {
    Name = "f5-auto management-a"
  }
}

resource "aws_subnet" "f5-management-b" {
  vpc_id                  = aws_vpc.f5-automation-vpc.id
  cidr_block              = "10.2.102.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "${var.aws_region}b"

  tags = {
    Name = "f5-auto management-b"
  }
}

resource "aws_subnet" "public-a" {
  vpc_id                  = aws_vpc.f5-automation-vpc.id
  cidr_block              = "10.2.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "${var.aws_region}a"

  tags = {
    Name = "f5-auto public-a"
  }
}

resource "aws_subnet" "private-a" {
  vpc_id                  = aws_vpc.f5-automation-vpc.id
  cidr_block              = "10.2.100.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "${var.aws_region}a"

  tags = {
    Name = "f5-auto private-a"
  }
}

resource "aws_subnet" "public-b" {
  vpc_id                  = aws_vpc.f5-automation-vpc.id
  cidr_block              = "10.2.2.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "${var.aws_region}b"

  tags = {
    Name = "f5-auto public-b"
  }
}

resource "aws_subnet" "private-b" {
  vpc_id                  = aws_vpc.f5-automation-vpc.id
  cidr_block              = "10.2.200.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "${var.aws_region}b"

  tags = {
    Name = "f5-auto private-b"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.f5-automation-vpc.id

  tags = {
    Name = "f5-auto IG"
  }
}

resource "aws_route_table" "rt1" {
  vpc_id = aws_vpc.f5-automation-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "F5-Auto Default"
    f5_cloud_failover_label = "mydeployment"
    #f5_self_ips tag used by f5 cloud failover iControl LX
    f5_self_ips = "${var.bigip1_private_ip[0]},${var.bigip2_private_ip[0]}"
  }
}

resource "aws_main_route_table_association" "association-subnet" {
  vpc_id         = aws_vpc.f5-automation-vpc.id
  route_table_id = aws_route_table.rt1.id
}

# commented out next two sections because the terraform templates don't consider that IAM doesn't have permissions to delete the log group and fails on destroy
#resource "aws_cloudwatch_log_group" "log-group" {
#  name = "f5-automation-log-group"
#}

#resource "aws_cloudwatch_log_stream" "log-stream" {
#  name           = "f5-automation-log-stream"
#  log_group_name = aws_cloudwatch_log_group.log-group.name
#}

resource "aws_security_group" "instance" {
  name   = "f5-auto-example-instance-sg"
  vpc_id = aws_vpc.f5-automation-vpc.id

  ingress {
    from_port   = var.server_port
    to_port     = var.server_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow the VPC to talk to the instance on port 3000
  ingress {
    from_port = 3000
    to_port   = 3000
    protocol  = "tcp"
    cidr_blocks = var.restrictedSrcAddressVPC
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_instance" "example-a" {

  #ami                       = var.web_server_ami
  ami                         = lookup(var.web_server_ami, var.aws_region)
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.public-a.id
  vpc_security_group_ids      = [aws_security_group.instance.id]
  key_name                    = var.aws_keypair
  private_ip                  = "10.2.1.80"
  
  # TODO: Move web app instances to private subnet and use NAT gateway for Internet access to pull docker image
  # Until then, this is required for the EC2 instance to get Internet access
  associate_public_ip_address = true
  
  user_data = <<-EOF
              #!/bin/bash
              yum update -y
              yum install -y docker
              /sbin/chkconfig --add docker
              service docker start
              docker run -d --net host -e F5DEMO_APP=website -e F5DEMO_NODENAME="Public Cloud Lab: AZ #1" --restart always --name juiceshop bkimminich/juice-shop:latest
              EOF

  tags = {
    Name   = "f5-auto-web-az1"
    findme = "web"
  }
}

resource "aws_instance" "example-b" {

  #ami                        = "{var.web_server_ami}"
  ami                         = lookup(var.web_server_ami, var.aws_region)
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.public-b.id
  vpc_security_group_ids      = [aws_security_group.instance.id]
  key_name                    = var.aws_keypair
  private_ip                  = "10.2.2.80"
  associate_public_ip_address = true
  
  user_data = <<-EOF
              #!/bin/bash
              yum update -y
              yum install -y docker
              /sbin/chkconfig --add docker
              service docker start
              docker run -d --net host -e F5DEMO_APP=website -e F5DEMO_NODENAME="Public Cloud Lab: AZ #2" --restart always --name juiceshop bkimminich/juice-shop:latest
              EOF

  tags = {
    Name   = "f5-auto-web-az2"
    findme = "web"
  }
}

resource "aws_security_group" "elb" {
  name   = "f5-auto-juice-elb"
  vpc_id = aws_vpc.f5-automation-vpc.id

  ingress {
    from_port   = 81
    to_port     = 81
    protocol    = "tcp"
    cidr_blocks = var.restrictedSrcAddress
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = var.restrictedSrcAddress
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "f5_management" {
  name   = "f5_auto_management_sg"
  vpc_id = aws_vpc.f5-automation-vpc.id

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = var.restrictedSrcAddress
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.restrictedSrcAddress
  }

  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = var.restrictedSrcAddressVPC
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.restrictedSrcAddress
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "f5_data" {
  name   = "f5_auto_data_sg"
  vpc_id = aws_vpc.f5-automation-vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = var.restrictedSrcAddress
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = var.restrictedSrcAddress
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.restrictedSrcAddressVPC
  }

  ingress {
    from_port   = 1026
    to_port     = 1026
    protocol    = "udp"
    cidr_blocks = var.restrictedSrcAddressVPC
  }

  ingress {
    from_port   = 4353
    to_port     = 4353
    protocol    = "tcp"
    cidr_blocks = var.restrictedSrcAddressVPC
  }

  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = var.restrictedSrcAddress
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.restrictedSrcAddressVPC
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
