# F5 Automation Example
## Getting Started
This project is based heavily off of a lab from F5 called [PC201 - Deploying F5 Solutions to AWS with Terraform and iControl LX Extensions](https://clouddocs.f5.com/training/community/public-cloud/html/class13/class13.html), though a lot of changes have been made to have it be functional outside of the pre-defined F5 lab environment.  It is inteded to provide a jumpstart to project teams wanting to automate usage of BigIPs into an application architecture with a functional example of how the automation toolchain from F5 supports that objective.  It is the "documentation" I wish I had when I wanted to better understand the automation toolchain from F5.

This project is intended to be used as an example of how building an F5 can be automated using Terraform and Ansible. <span style='color:red'>The resources in this project **SHOULD NOT** be directly used to implement F5 BigIPs in a production environment</span>.

    NOTE: This example directly configures the BigIP devices that are created in AWS. If BigIQ is being used in an environment to centralize configuratino of BigIPs the examples are likely very applicable but may need to be routed through BigIQ.

## What this project is NOT

1. NOT a hardened deployment architecture for BigIP devices and a web application.  The architecture is one that facilitates the demo, not a model for any real use.  The most that can be taken from the architecture deployed by this demo to AWS is an understanding of how the F5 Cloud Failover extension functions in AWS (EIP and S3).
2. NOT an example of a pipeline. Even though the project is in gitlab, this is NOT an example of how to deploy an application with BigIP devices via a pipeline. Though it is the hope that this project will jumpstart those efforts, shortening the time it takes to learn the automation toolchaing provided by F5 and the F5 configuration.  There isn't a pipeline defined for this project and the pipeline defined in the WAF policy sibling project is also only a means to an end for demonstration purposes.
3. NOT an example of how to use the F5 automation toolchain in a fully hardened fashion.  Some care was taken to not hard code secrets, but they are drawn from environment variables as a simple way to address that problem.  Real secrets management should be used to provide the secrets needed to build/deploy an application.  In some cases it may be appropriate for the pipeline to pull the secrets and provide them to the automation.
4. NOT a fault tolerant implementation of the automation tools. Some effort was taken to make the demo somewhat resilient to failures with a little bit of error handling. Far more work would need to be done in an real implementation to do a better job of handling errors that could come up. A live demo could fail as many error conditions have not been accounted for.

## Architecture
This is a rough diagram of the architecture implemented by this demo.

![](/assets/images/F5-Automation-Architecture.png)

    NOTE: This is NOT an example of how to properly architect or harden an implementaiton of BigIP devices nor a web application. The archtiecture is a replication of what was done in the F5 hands-on lab and is designed for the purposes of demonstrating the tooling. This architecture should not be directly used for a real implementation.

***

## Security Controls Demonstrated
This demo provides examples of how to configure a BigIP device with the following security controls via automation:

1. Restricting access to the management interface of a BigIP (SSH and HTTP) using both:
    1. AWS Security Group
    2. BigIP Management Firewall Rules
2. Daily checks to auto update AWAF threat intelligence components on BigIP
    1. ASM Attack Signatures
    2. Bot Signatures
    3. Browser Challenges
    4. Server Technologies
    5. Threat Campaigns
3. Specific cipher suites accepted for connections to a web application
4. Custom server certificate signed by an internal CA for a web applicaiton, including a passphrase protecting the private key
5. Custom device certificates used by the BigIPs
6. AWAF (formerly ASM) policy protecting a web application including (not a comprehensive list):
    1. Server Tecnologies
    2. Allow list of response codes
    3. Threat Campaigns
    4. IP Intelligence
    5. Allow list of file types
    6. Session and Logins
    7. Content profiles (JSON validation)
    8. HTTP protocol compliance
    9. Attack signature sets scoped to the technology stack with medium or higher risk and medium or higher accuracy
    10. Allow list of URLs
    11. Allow list and validation of parameters
    12. Allow list of HTTP methods
    13. Redirect protection
    14. CSRF protection
7. Bot protection for a web application
8. DDoS protection for a web application

<pre>NOTE: This is NOT an example of a fully hardened implementation for the configuration of a BigIP or a web application. This demo is limited to providing an example to jumpstart adding a BigIP to provide controls protecting an application via automation.</pre>

## BigIP Cloud Features Demonstrated
Not a comprehensive list of cloud features offered by BigIP, this demo provides examples of how to configure and utilize the following capabilities of a BigIP in an AWS environment:

1. <u>Cloud failover</u>: With BigIP devices in an active/standby configuration and the F5 Cloud Failover extension installed/configured on both BigIP devices, an AWS EIP points to the active BigIP device. When the active BigIP fails over to the standby, the BigIP automatically reconfigures the EIP to point to the standby that is now made active. This eliminates the need to have an AWS load balancer in front of the active/standby BigIP devices.

    <pre>NOTE: GTM could replace and/or significantly change the use of cloud failover</pre>

2. <u>Auto discovery of application nodes</u>: When tagged properly application nodes are automatically discovered by the BigIP device and added to the application pool.

***

## Basic Terms/Principles

### <u>Imperative vs Declarative</u>
The terminology may not be the same throughout all of the automation landscape, but to F5 these have very specific meanings that are seen throughout documentation and training.

`Idempotent` is a term common to the automation world. It means that a command can be run multiple times without changing the result. So long as the command isn't changed and the system executing the command hasn't changed, nothing will change if you execute the command over and over.

`Imperative` commands are those made one at a time by a human or a script.  They do not know about dependencies on other objects or take into account the results of previous operations.  It is left to the caller of the command to undo the command if needed. Impertaive commands to a BigIP are not idempotent, they will make changes to the BigIP even if the change being made leaves the BigIP in exactly the same state.

There are some examples of using imperative commands in this project, but they are only here to show how they can be used.  In general F5 strongly discourages the use of imperative commands.

`Declarative` commands allow for a group of instructions to be given all at once.  Provided the group of instructions pass a schema check to validate the syntax is correct, it is left to the system that receives this group of instructions to know how to run them in the correct order and rollback everything that was started should any one of them fail.  Declarative commands are idempotent.  If the instructions in the declarative JSON file do not change and the BigIP has already executed the commands in the decalartive JSON file the declarative command can be executed over and over with no change to the BigIP.

### <u>Declarative Onboarding vs Application Services</u>
F5 offers 2 different declarative extensions for configing a BigIP device (physical or virtual).

`Declarative Onboarding (DO)` is the extension that is used to configure the BigIP device itself.  Things like hostnames, interfaces, Self IPs, VLANs, trunks, DNS, NTP.  The core things you have to do after bring up a new BigIP to make it functional as a networking device.

`Application Services (AS3)` is the extension that is used to configure the application elements of a BigIP device.  Things like VIPs, pools, nodes, TLS certificates, load balancing, ASM, APM.

Both DO and AS3 implement declarative commands.  They accept a group of instructions by means of JSON files that have to conform to well defined and specific schemas.  The DO and AS3 schemas and extensions are very different.  DO and AS3 are versioned indepently from each other and really know nothing about each other.  DO needs to be done prior to any AS3 as AS3 depends a lot on the foundational elements of DO being completed in order to function.

The DO and AS3 extensions are under very active development from F5.  There is not full parity between the things that can be done in the web-based UI (tmui) or via command line (tmsh) in these extensions.  You can see a few exceptions here in this project.  When the command is not available via DO/AS3 you can look to see if it is available as an imperative command or if you can script it in tmsh.

***

## Process of This Demo

This demo creates a system using AWS components.  Terraform is used to create/destroy those components in AWS.  Ansible is also capable of creating the required AWS components, but Ansible takes more of an imperative approach while Terraform uses more of a declarative approach that is friendlier to CI/CD pipelines.  Some work has been done to try and make the Terraform configuration is idempotent, but this is something that will need to be looked at thoroughly as the configuration is adapted for real use cases.  Idempotent Terraform will be especially important if the examples in this project are going to be adapted into a CI/CD pipeline (e.g. Gitlab) where the creation/destruction of BigIPs should happen rarely.

After the Terraform commands create all of the AWS components there will be 2 BigIP devices with only the little bit of configuration defined in the cloud-init (user data in AWS specific terms) portion in Terraform. When Terrforam is applied, the BigIP devices remain non-functional.  For the purposes of this demo this project uses Terraform provisioner commands to invoke the Ansible roles that use F5 extensions to configure the BigIPs. Terraform provisioner commands are not best practice as Terraform doesn't know the impact/result of those commands. If this example is being adapted to a CI/CD pipeline it would be better to have the pipeline do the Terraform commands and then do the Ansible commands.

The Ansible roles do the following with the BigIPs

1. Set the password for the admin account on the BigIPs.  In a real use case this would be better done via cloud-init scripts that pull the password from secrets management.
2. Install the F5 DO, AS3, and CFE exensions needed for the subsequent Ansible roles
3. Configure the basic networking and active/passive sync of the BigIPs via the DO extension. The ASM module (still called ASM in the BigIP is provisioned in this step)
4. Configure the Live Update schedule to check for, download, and install AWAF threat intelligence components updates daily
5. Configure cloud failover for AWS between the two BigIPs via the CFE extension
6. Apply custom certificates as device certificates to each BigIP
7. Configure the Juice Shop application (including AWAF policy) in the BigIPs via the AS3 extension

Rough time for the steps:

1. Terraform creates all of the AWS components: 4 minutes
2. EC2 instances running BigIP are fully initialized: 1 minute
3. Password set and extensions installed: 2 minutes
4. BigIPs functionally configured and setup in HA active/standby pair: 3 minutes
5. Cloud Failover configured: 1 minute
6. Device certificates applied: 8 minutes (rolling reboots of BigIPs, most likely failure point of this demo)
7. JuiceShop application created in BigIPs: 30 seconds

Total Time: About 20 minutes

***

## Resources
- F5 Ansible "bigip" imperative modules: https://docs.ansible.com/ansible/latest/collections/f5networks/f5_modules/index.html

    <pre>NOTE: F5 highly discourages the use of the imperative Ansbile modules, most of which are not idempotent, in favor of declarative configuration. The trouble is that some of the declarative configuration (especially with declarative onboarding) doesn't work correctly.  An example is with creating device trust between peer F5 BigIPs instances.  Declarative onboarding can create the failover group fine, but as of late 2021 failed to create device trust.  In general, declarative configuration (DO, AS3) should be used first and imperative should be done only when that fails.</pre>

- F5 Declarative Onboarding (DO) Documentation: https://clouddocs.f5.com/products/extensions/f5-declarative-onboarding/latest/
- F5 DO Schema Reference: https://clouddocs.f5.com/products/extensions/f5-declarative-onboarding/latest/schema-reference.html
- F5 DO JSON Validation in VSCode: https://clouddocs.f5.com/products/extensions/f5-declarative-onboarding/latest/validate.html
- F5 Application Services (AS3) Documentation: https://clouddocs.f5.com/products/extensions/f5-appsvcs-extension/latest/
- F5 AS3 Schema Reference: https://clouddocs.f5.com/products/extensions/f5-appsvcs-extension/latest/refguide/schema-reference.html
- F5 Telemetry Streaming (TS) Documentation: https://clouddocs.f5.com/products/extensions/f5-telemetry-streaming/latest/
- F5 Cloud Failover (CFE) Documentation: https://clouddocs.f5.com/products/extensions/f5-cloud-failover/latest/
- F5 BigIP Cloud Failover in AWS: https://clouddocs.f5.com/products/extensions/f5-cloud-failover/latest/userguide/aws.html#aws
- F5 ConfigSync through tmsh: https://support.f5.com/csp/article/K14856
- F5 Live Update configuration through tmsh: https://support.f5.com/csp/article/K94125220
- Why the LOCAL_ONLY parition (required for AWS cloud failover): https://support.f5.com/csp/article/K45921315
- The /var/log/restnoded/restnoded.log log file on the BigIP shows a lot of good logging related to most of the actions taken via the F5 automation tool chain.  If you are struggling to make something work, ssh to the BigIP and tail that log.

***

## Prerequisites
In order to execute this project on your EWS workstation you will need to have the following installed on the system that will exeucte the Terraform plan and Ansible playbooks:

1. [AWS CLI v2](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html)
2. [Terraform 0.12](https://releases.hashicorp.com/terraform/0.12.31/) (F5 has confirmed that this should work with terraform 0.11 but I have used 0.13.7 in my examples)
3. python 3.x
4. [Ansible 2.9+](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#selecting-an-ansible-artifact-and-version-to-install). F5 recommends v2.9+.  F5 also recommends installing ansible via [PIP](https://clouddocs.f5.com/products/orchestration/ansible/devel/f5_modules/getting_started.html)

    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    python3 get-pip.py --user
    python3 -m pip install --user ansible

5. paramiko as the connection plugin for ansible installed via PIP as well 

    python3 -m pip install --user paramiko

6. [F5 ansible modules](https://galaxy.ansible.com/f5networks/f5_modules)

3. A [CA bundle](https://docs.aws.amazon.com/sdkref/latest/guide/setting-global-ca_bundle.html) with the root and intermediary certificates from the EWS proxies configured in ~/.aws/config or as AWS_CA_BUNDLE environment variable

    NOTE: When using the CA bundle with EWS proxy certificates you may see a warning message similar to the following:

    <pre>urllib3/connectionpool.py:1013: InsecureRequestWarning: Unverified HTTPS request is being made to host '{aws_service}.{aws_region}.amazonaws.com'. Adding certificate verification is strongly advised.</pre>
    
    This can be ignored while running things from an EWS workstation

4. AWS account credentials (see AWS CLI With MFA Protected User Account below on how to use an AWS IAM user protected by MFA)

5. Set the following Environment variables
    - AWS_ACCOUNT_ID: The 12 digit numeric account ID for your AWS Account
    - AWS_IAM_ID: The IAM id from your AWS Account for your user.  Most likely is your EWS email address
    - AWS_ACCESS_KEY_ID: The Access Key ID to your AWS account
    - AWS_SECRET_ACCESS_KEY: The Secret Access Key to your AWS account
    - emailid: Your EWS email address (yes, may be the same as AWS_IAM_ID, but want to make sure we have this specifically for tagging/naming resources in AWS)
    - emailidsan: Your EWS email address without the punctuation. Example: `emailid`=First.Last@earlywarning.com `emailidsan`=firstlastearlywarningcom
    - bigip_admin_password: The password that should be set on the F5 for the `admin` user and used with all other calls to automatically configure the BigIP
    - restricted_ip_address: The IP address that should be used to restrict access of the BigIP management interface and the Juice Shop Application so that neither is publicly available. Example: 99.99.99.99

6. Subscription to allow the "`F5 Advanced WAF with LTM, IPI, and Threat Campaigns (PAYG, 25Mbps)`" for your AWS account.  This is the smallest F5 BigIP AMI that will enable all of the example configuration in this demo to function. The 25Mbps licensing is likely NOT sufficient for production usage. You can check to see if the subscription is already enabled [here](https://console.aws.amazon.com/marketplace/home?#/subscriptions).  If this specific AMI is not yet available, do the following:

    - Search for "f5 advanced 25mbps" in the [AWS Markteplace](https://console.aws.amazon.com/marketplace/home?region=us-east-1#/search!mpSearch/search)
    - Choose the "`F5 Advanced WAF with LTM, IPI, and Threat Campaigns (PAYG, 25Mbps)`".  **Advanced** or "**AWAF**" is the key thing.  Should look something like this
        ![](/assets/images/F5-AWAF-25mbps.png)
    - Continue to Subscribe
        ![](/assets/images/ContinueToSubscribe.png)
    - Accept Terms (30 day trial starts as soon as you push this button)
        ![](/assets/images/AcceptTerms.png) 
    - It can take a while for AWS to process your request to allow this subscription in the marketplace, though it only took a few minutes when I did it. Got an email saying it had been enabled.
    - NOTE: If you don't see the subscription applied to the account, you may need to be provisioned an AWS License Manager serivce linked role (SLR) - OR - have an AWS admin add the subscription for you.
        ![](/assets/images/Subscription_SLR.png)

7. Entitlement to launch `m5.xlarge` EC2 instances for the F5 BigIP and `t2.micro` EC2 instances for sampe web servers under the F5.

    NOTE: The m5.xlarge instance type is the smallest size F5 recommends for running BigIP instances. Depending on the throughput needs of the application (something Telemetry Streaming can help define during testing), larger instances may be needed.

8. 3 EIPs.  2 to assign as the public management address of the BigIPs. 1 for the Juice Shop application. Create the EIPs in AWS yourself, then update the bigip-vars.tf or set these environment variables:
    - TF_VAR_bigip1_mgmt_ip: The public IP address defined by the EIP for bigip1. Example: eipalloc-00b75a9287c6ddca5
    - TF_VAR_bigip2_mgmt_ip: The public IP address defined by the EIP for bigip2. Example: eipalloc-00b75a9287c6ddca5
    - TF_VAR_juice_shop_eip_id: The public IP address defined by the EIP for the Juice Shop application. Example: eipalloc-00b75a9287c6ddca5
    - TF_VAR_cloud_failover_label: The label that will be used by the F5 cloud failover extension (CFE) to send/recieve failover information in S3 bucket.  Example: ewsseclabdemo 

***

## Basic Steps
1. Customize the `start.sh` script to **set your email** and then source it to add scripts to the path and setup some handy aliases (source is to make sure this works on Mac) 

      source start.sh

2. If using an MFA protected IAM user generate your temporary credentials passing in a current MFA token (changes would have to be made to support a different kind of user)

        get-aws-session-token.sh {mfa_token}

3. If using an MFA protected IAM user and running these scripts on a mac, source the resulting temporary aws credentials

        source .aws-creds

4. Generate ssh keys that will be used to ssh to the EC2 instances that are built by terraform

        create-ssh-keys.sh

    NOTE: Validate the keypair shows up as an EC2 KeyPair in the region where you are doing this work.  The console page should be something like https://us-west-2.console.aws.amazon.com/ec2/v2/home?region=us-west-2#KeyPairs:

5. Generate private root CA and sign custom server certificate

        create-private-ca-certs.sh

6. Configure the IP address ranges setup in `main.tf`, and `vars.tf`.  Without being changed 10.2.0.0/16 addresses will be used in AWS
7. Run the terraform init command to have terraform download the necessary add-ons:

        terraform init

    Should get a **<span style=color:green>Terraform has been successfully initialized!</span>** message

8. Run the terraform validation command to see that the terraform files are syntactically correct:

        terraform validate

    Should get a "**<span style=color:green>Success!</span>** The configuration is valid." message.

9. Run the terraform plan to see what terraform thinks has to be done to implement in your AWS account

        terraform plan

    Where `{password}` is a value you choose now that will be used to setup the password for the admin account of the F5 GUI Console.  If you don't include the -var parameter then the script will ask you to provide the value as you run the plan command.

    If you have everything configured to this point you should see a list of changes that terraform would make to your AWS environment if you were to execute the plan.  If you have never run the sript before you should see something like "**Plan**: 53 to add, 0 to change, 0 to destroy."

10. Take a look at the terraform plan and see that things look right.  Double-check the IP addresses the are going to be used when creating a VPC.

11. Run the terraform apply command to have terraform create the VPC, subnets, and EC2 instances need for the F5 automation example to function:

        terraform apply

        NOTE: The terraform script invokes some ansible scripts to do the onboarding and configuration of the BigIPs

12. If completed successfully you should see:
- AWS
    - A new VPC
    - Several new subnets
    - New security groups
    - 2 EC2 instances running BigIP
    - 2 EC2 instances running the Juice Shop web application
    - 3 EIPs. 2 pointing to each management interface and 1 for the Juice Shop application
    - 1 ALB that demonstrates how it could be setup, though using an EIP and the load balancing capabilities of the BigIP will be much better (especially with regard to switching between the active and standby BigIP)
- BigIPs
    - "admin" password set to provided value
    - "admin" default shell set to "bash" (required for some of the automation to function)
    - Basic networking (DNS, NTP, default route, self ip, VLANs) configured
    - Device trust established between the two BigIPs
    - Failover group (primary/secondary) defined with `bigip1` as the active and `bigip2` as the standby
    - Management UI for both BigIPs restricted to provided IP address ranges even though they have public IP addresses. This is done with the AWS security group and firewall rules on the BigIPs.
    - Custom certificates are applied to each BigIP Device
    - Juice Shop application defined with VIPs, Pools, HTTPS cipher suites, custom certificate signed by internal CA, AWAF policy, DOS Profile, Bot protection.
    - The pools defined to facilitate both active/standby function of the BigIP and to dynamically find EC2 instances added to the AWS account.
    - Cloud failover defined through AWS so that the EIP in front of the Juice Shop application will update to the active BigIP within a few seconds when active goes to standby.

13. The Telemetry Streaming (TS) extension is not configured as an example by this project. The TS extension should be pursued for helping understand the resource utilizaiton of the BigIPs and applications.  Specifically configuring the metrics to flow to Splunk.

***

## Project Files Explanation

There are a lot of files in this project that were created as examples of how to use some of the older automation tools offered by F5 but are no longer recommended as best practice. To make it clear which are the files used in this demo that follow F5 best practice as far as automation goes (not demonstrating best practice for a fully hardened implementation) here is a list of the files in this demo project and what they do listed in alphabetic order.

- `ansible` folder: Contains a number of Ansible playbooks and roles. Some of the files and roles are examples of older automation tools for F5 that are no longer recommended
    - `collections` folder: Not used
    - `roles` folder: A number of Ansible roles that allow for the defintion of re-usable/shared tasks, a best practice for doing Ansible commands that may span across playbooks
        - `aws_wait_for_bigip_instance` folder: Example of how to use Ansible to make a call from the ansible host (the machine running the ansbile command) to AWS to check the status of an EC2 instance until it shows as being initialized. This assumes that the ansible host can run a shell command where the AWS CLI is installed and connect over the Internet to AWS. This is a really good way to make sure other Ansible tasks don't execute until AWS EC2 isntances are fully initialized rather than just waiting for a few minutes

        - `bigip_backup_ucs` folder: Example of how to use the imperative Ansible task provided by F5 to make a backup of a BigIP configuration and download that backup to the ansible host. This is an imperative Ansible task but F5 doesn't provide a declarative way to do this. If a configuration backup is needed this is a good way to do that. 
        
            <pre>NOTE: This command takes several minutes to work. The more configuration there is in the F5 the longer it takes.</pre>

        - `bigip_create_device_trust` folder: <b>NOT BEST PRACTICE</b>. Example of how to use this imperative Ansible task that is NOT idempotent. The DO extension should be used.

        - `bigip_onboarding` folder: <b>NOT BEST PRACTICE</b>. Example of how to use a few imperative Ansible tasks to do a number of BigIP basic configuration. Notice that the Ansible tasks in this role have a very specific order and none of them are idempotent. The DO extension should be used.

        - `bigip_restore_ucs` folder: Like the bigip_backup_ucs this is an example of how to restore from a UCS file located on the ansible host. The Ansible task is imperative but if there is a need to restore from a UCS this is a good way to do that.  NOTE: This task takes several minutes to execute.

        - `f5_as3` folder: Example of an Ansible role that has tasks that can be re-used across Ansible playbooks to send AS3 JSON files to BigIP devices. The role assumes that the AS3 extension is already installed on the BigIP.  This is the F5 best practice method for automating the creation/updating of an application in a BigIP.  The Ansible task is declarative and idempotent.  

            <pre>NOTE: The Ansible built in "uri" module is used in this role to use the Application Services (AS3) extension provided by F5. AS3 is used via HTTP REST commands and therefore doesn't need anything more than the the Ansbile uri module. Some error handling is defined in the role but more work should be done to handle more conditions as the role did fail in some of the testing.</pre>

        - `f5_declarative_onboarding` folder: Example of an Ansible role that has tasks that can be re-used across Ansible playbooks to send DO JSON files to BigIP devices.  This assumes that the DO extension is installed on the BigIP.  This is the F5 best practice method for automating the internal configuration of a BigIP required to make it functional.  The Ansible task is declarative and idempotent.

            <pre>NOTE: The Ansible built in "uri" module is used in this role to use the Declarative Onboarding (DO) extension provided by F5. DO is used via HTTP REST commands and therefore doesn't need anything more than the the Ansbile uri module. Some error handling is defined in the role but more work should be done to handle more conditions as the role did fail in some of the testing.</pre>

        - `f5_install_extension` folder: Example of an Ansible role that has tasks that can be re-used across Ansible playbooks to install F5 extensions on BigIP devices.  The task checks to see if the extension is intalled, but doesn't check the version of the extension, making this role unsuitable for upgrading the extensions on BigIP devices.  The extension version isn't checked due to a limitation in Ansible where jinja variables can't be compared to one another. 
        
            <pre>NOTE: As of the creation of this project F5 didn't offer anything in the automation toolchain to support the installation of the extensions. Most of the F5 examples do this via SSH commands that execute sripts to on the BigIP to download and install extensions. In this demo the extensions are downloaded to the ansible host, uploaded to a temp directory allowed by default by a BigiP, moved to a location on the BigIP required for installation, and then installed using an iControl (installed by default) REST call fromm that allowed location. Some error handling is defined in the role but more work should be done to handle error conditions as the role did fail in some of the testing.</pre>

        - `f5_set_asm_update_freq` folder: Example of how to change the Live Update of the AWAF threat intelligence information (attack signatures, threat campaigns, bot detection, and technology stacks). F5 does a lot of threat intelligence gathering themselves and with customers. That experience results in frequent updates to the protection that can be provided via AWAF policy to applications. There isn't a fixed schedule for these updates. Rather the updates are done based on the threats obvserved. F5 best practice is to enable live update of these threat intelligence components automatically daily. AWAF policy allows for incorporating these updates in safe ways so that they are not automatically applied without human involvement (staging) if needed. 
        
            <pre>NOTE: Setting live update do check for updated threat intelligence components is done via tmsh commands executed over SSH because F5 did not provide a way to do this via the automation toolchain as of the creation of this demo. Also note that to execute tmsh commands using Ansible the built in "raw" module has to be used instead of the built in "ssh" module.  This is due to the Ansible requirement that python be installed on the remote host in order to use the "ssh" module.</pre>

        - `ssh_set_bigip_admin_password` folder: <b>NOT A HARDENED EXAMPLE</b>. In order to have any of the BigIP configuration/update automation function, the Ansible scripts need to use the admin user on the BigIP device.  Some of the tasks will work under a new user with an admin role, but a few of them will only function under the default admin role. The password for the admin user has to be set to known value and this Ansible role does that by using the AWS KeyPair that allows SSH access to the EC2 instance running BigIP. Without doing more work to use AWS secrets management, or other solutions like Vault, this was the easiest way to get the automation in this demo project functional.  It would be much better to set the password for the admin user on the BigIP to a known value in the Terraform cloud-init (see bigips.tf) where the script pulls the password from a secrets  manager.

    - `Apply-Device-Certificate.yml`: Example of best practice method for updating the device certificate for a BigIP.
        
        <pre>NOTE: This process forces a reboot of the BigIP. If the JSON sent to DO includes more changes than just updating the certificate the device will use, there is a high risk that those changes will not happen due to the reboot.  Therefore it is advised that device certificates changes be the only thing that changes in the JSON of a DO.</pre>

    - `Apply-Netowrking-Imperative.yml`: <b>NOT BEST PRACTICE</b>. Exmaple of how to use imperative Ansible modules provided by F5 to do much of the basic configuration of a BigIP to make it function. Declarative Onboarding should be used instead of these imperative Ansible modules.

    - `Apply-Netowrking.yml`: Example of best practice method for doing the basic configuration of a BigIP via Declarative Onboarding. Some effort was made to do error handling but more should be done to make sure that various responses from the BigIP are handled well in a CI/CD pipeline.

    - `as3-juice-shop-template.json`: Example of a JSON template that can be sent to the F5 AS3 extension to define the Juice Shop application with TLS (including custom certificates signed by an internal CA).

        <pre>NOTE: Care should be taken to make sure that the JSON data used with AS3 contains all of the elements of the application you want to configure.  If a subsequent call is made to the AS3 extension where JSON data from previous calls is not included, the BigIP will remove that part of the configuration.</pre>

    - `BackupBigIPs.yml`: Example of how to use the imperative Ansible module provided by F5 to make a backup of a BigIP.  Even though it is imperative, there isn't a better way to do this with a BigIP.

    - `cloud-failover-template.json`: Example JSON template file used in conjunction with the Cloud Failover extension provide by F5 to define how the BigIP should execute a failover to have a standby become active. This example is specific to how this is done in AWS using an EIP.  The file is a template because there are variables in the file that are replaced via Terraform to include information that may change as the environment is built out.

    - `Create-Juice-Shop.yml`: Example of best practice for defining a web application in a BigIP via the Application Services extension. The `f5_as3` role does most of the work with this yaml file defining the few variables that may be different from application to application.

        <pre>NOTE: There is some special work done here to make a private key that is part of a server certificate and protected by a passphrase work in AS3. The F5 documentation was not very helpful to working through that specific use case. Check out the create-private-ca-certs-password.sh script, this file, and then the as3-juiceshop-template.json file to see how the formatting requirements all came together.</pre>

    - `Define-Cloud-Failover.yml`: Example of best practice for using the Cloud Failover extension to define how an active BigIP should transition to be standby and the standby becomes active. Different from using the other extensions, the use of the F5 Cloud Failover (CFE) extension is done entirely in this one file using Ansible tasks. Using CFE isn't much different from the DO or AS3 extensions, this was done differently just to provide an alternative example of how the REST calls can be made to the BigIP without using an Ansible role. In general it is best practice to use Ansible roles if there is any chance the tasks that are called may be re-used or shared across different playbooks. With the DO and AS3 extensions it is more likely that the tasks will be used across multiple Ansible playbooks than with the CFE extension.

    - `do-basic-networking-template.json`: Example of a JSON file that can be sent to a BigIP via the Declarative Onboarding extension and configure the most basic components of a BigIP to make it functional. This is a template file because there are variables defined in the file that could be replaced by information that may be specific to an environment being built. Components like DNS, NTP, default route, self IP, VLANs, etc. This file is not used in the demo because the slightly more complicated example that adds device trust and active/standby was what we wanted to demonstrate and you can define it all at once (the beauty of declarative configuration over imperative).

    - `do-device-sync-template.json`: Example of a JSON file that can be sent to a BigIP via the Declarative Onboarding extension and configure a BigIP to make it functional. This is a template file because there are variables defined in the file that are replaced by information specific to an environment being built and deployed. This template includes the definition of basic components like DNS, NTP, default route, self IP, VLANs, etc. It also includes the definition to establish device trust and active/standby between two BigIPs.

        <pre>NOTE: In general care should be taken to make sure that the JSON data used with DO contains all of the elements of the BigIP that you want to configure and assume that elements left out in subsequent calls to DO may have the BigIP remove configuration not included.  Though in testing it seemed that some of that wasn't always true.</pre>

    - `do-restrict-mgmt-interface-example.json`: Example of a JSON file thet can be sent to a BigIP via the Declarative Onboarding extension and define firewall rules for the management interface to restrict access. This is a template file because there are variables defined in the file that could be replaced by information that may be specific to an environment being built. This file is not used in the demo because the management interface is restricted to IP addresses that should have the access via the cloud init  (cloud-init-bigip-template.yml) scipt executed by AWS as the EC2 instance is initialized.

    - `Imperative-Setup-Example.yml`: <b>NOT BEST PRACTICE</b> Example of how a number of imperative Ansible modules provided by F5 could be called in successive tasks to do the basic configuration of a BigIP to make it functional. The example is incomplete, meaning it won't result in a functional BigIP, and is provided only as an example of how Ansible tasks could be used to do this.  Declarative configuration via the DO and AS3 extensions is the preferred way to configure a BigIP.

    - `Install-F5-Extensions.yml`: Example of how Ansible tasks can be combined with the `f5_as3` role to define the version variables for the F5 extensions needed for the F5 automation toolchain to function.

    - `juice-shop-template-no-tls.json`: Example of a JSON template that can be sent to the F5 AS3 extension to define the Juice Shop application without TLS.  This is a template because it has variables that could be replaced by Terraform with build/environment specific information. This file is not used in the demo as TLS was something that we wanted to demonstrate.

    - `juice-shop-template.json`: Example of a JSON template that can be sent to the F5 AS3 extension to define the Juice Shop application with TLS (including custom certificates signed by an internal CA).  This file is no longer used in the demo in favor of having the Ansible f5_as3 role replace the data in the `as3-juiceshop-templat.json` template.  This made testing the way certificates were created and using a passphrase for the private key easier.

- `assets` folder: Image used in this readme

- `generated` folder: A place for temporary files are stored. Renered template files where variables have been replaced by the information specific to this build/deploy. State files used by several Ansible playbooks to prevent tasks from running a second time in an attempt to make things a little more idempotent.  Mostly this folder is used to facilitate the demo and make it possible to make changes in the later steps without having to tear everything down and start at the beginning to test those changes. 
    
    A CI/CD pipeline combined with a focus on making the automation idempotent with every step should remove the need to have this folder be used by this process.

- `postman` folder: Some postman collections provided by F5 in the lab this demo was based on. The lab had learners use Postman to make the calls to the DO and AS3 extensions instead of Ansible. The folder is included so that the examples were available to anyone looking at this project but none are used by the demo.

- `scripts` folder: Shell scripts that were part of the F5 lab this demo was based on, though some scripts were added to enable this demo as noted here.  Most of these scripts are not used by the demo.

    - `ansible-install-f5-extensions.sh`: Example of how a script could execute an Ansible playbook.  Not part of the F5 lab.  Not used in the demo.
    - `bigip-do-network-modules.sh`: Example of how a script could be used to use the DO extension and configure a BigIP instead of using the Ansible tasks or roles. Not part of the F5 lab.  Not used in the demo.
    - `bigip-install-*.sh`: Examples of how a script could be used to install an F5 extension instead of using the Ansible tasks or roles. Not part of the F5 lab.  Not used in the demo.
    - `bigip-set-admin-password.sh`: Example of a how a script could be used to set the password of the admin user on a BigIP device. Not part of the F5 lab. Not used in the demo.
    - `bigip-upload-*.sh`: Examples of how a script can be used to upload an rpm to an BigIP so that it can be installed by other calls.
    - `bigip-verify-do.sh`: Example of how a script can be used to get the current configuration of a BigIP and/or validate that the DO extension has been installed. Not part of the F5 lab. Not used in the demo.
    - `cleanup.sh`: Script to cleanup the project and "reset". Provided by the F5 lab, customized with this demo. Not used in the demo.
    - `create-blank-ecdsa-certs.sh`: Creates a few blank files. Was part of the F5 lab. Not used by the demo.
    - `create-ecdsa-certs.sh`: Creates self-signed ceritifcates. Was part of the F5 lab. Not used by the demo.
    - `create-private-ca-certs.sh`: Creates a root certificate for a private CA and then generates and signs a server certificate that can be used with the Juice Shop application and the BigIP Management UI. None of the keys are protected by a passphrase, which was the only way it would work with AS3 for a while. AS3 requires the certificates, keys, and passphrase in a very specific format that is not well documented, but after finally getting it to all function this script is no longer used in the demo in favor of the `create-private-ca-certs-password.sh` script.
    - `create-private-ca-certs-password.sh`: Creates a root certificate for a private CA and then generates and signs a server certificate that can be used with the Juice Shop application and the BigIP Management UI. The private keys for the CA and the certificate for the Juice Shop app are protected by a passphrase which AS3 allows (though it requires a VERY specific format). DO doesn't allow for a private key protected by a password as of now.
    - `create-ssh-keys.sh`: Creates a KeyPair that can be registered with AWS and allow key based access to EC2 instances. Provided by the F5 lab. Used in the demo to generate a KeyPair as part of the Terraform process
    - `create-ssh-to-bigip-alias.sh`: Creates aliases helpful for connected to EC2 instances via SSH on a command line. Provided by the F5 lab. Not used in the demo.
    - `create-terraform-dependency-graph.sh`: Creates a graphic that shows just how complicated the build out of the AWS environment would be if you were to attempt it manually. Provided by the F5 lab. Not used in the demo.
    - `get-aws-session-token.sh`: Requires an MFA token as a parameter and then makes the AWS calls via the AWS CLI to get temporary credentials needed for Terraform to function. Not part of the F5 lab. Required in the demo if your AWS user requires MFA.
    - `print-aws-console-credentials.sh`: Prints out the AWS credentials used with Terraform. Provided by the F5 lab. Not used in the demo (and not really a good idea).

- `bigip-iam-role.tf`: Defines the IAM role needed for the EC2 instances running BigIP for Cloud Failover to function.  The IAM role is built every time the Terraform builds the environment and destroyed when the environment is torn down. If applied to a real pipeline it may be better to define that role outside of Terraform and reference it so that it is persistent and maintained by Security, but that will have to be worked out between engineering, delivery, and security.

    <pre>NOTE: The permissions defined in this file are overly permissive and MUST NOT BE USED in a real deployment.  See https://clouddocs.f5.com/products/extensions/f5-cloud-failover/latest/userguide/aws.html#create-and-assign-an-iam-role</pre>

- `bigip-outputs.tf`: Prints information that is unique to each run of the Terraform process out to the console. This was provided in the F5 lab and was helpful for manual SSH calls and Postman invocations. Not needed outside of that lab experience. Not used in the demo.

- `bigip-vars.tf`: Defines environment specific variables that will be used by Terraform and Ansible as the automation executes. In this demo those variables are defined via shell environment variables. If this automation is being applied to a CI/CD, the variables should be defined in the pipeline.

- `bigips.tf`: The Terraform resources that will be built as the plan is applied. The calls out to Ansible to configure the BigIPs are also included as "provisioner" scripts as a convenience for the purposes of having the demo be fully automated. If the example is being adapated to a real CI/CD implementation the pipeline should make the Ansible calls.

- `cloud-init-bigip-template.yml`: The yaml file that is executed by the EC2 instances running BigIP. This is an important point where security controls can be implemented as soon as possible like setting the password of the admin user (which is not done in this demo). This is a template file because there are some variables that are replaced as Terraform as applied with build/deploy specific information.

- `config.template`: Creates a shortcut on a desktop. Was provided by F5 lab. Not used in the demo.

- `inventory_template.yml`: Yaml formatted Ansible inventory file that defines a number of attributes per host needed as Ansible tasks and roles are executed in a playbook. This is a template because variables in the file are replaced by build/deploy specific information as part of the automation.

- `main.tf`: The starting point for Terraform. Provided originally by the F5 lab, but heavily customized for the purposes of this demo. The splitting out of Terraform resources in bigips.tf from this file is not required but is helpful to organize things to make it clear which resources are for the bigips and which are more generic.

- `outputs.tf`: Defines variables that are printed to the console after Terrform apply finishes successfully. Provided by the F5 lab. Was helpful as part of the lab, but not really necessary for the demo nor real use cases.

- `postman_template.json`: Postman environment template file. Can be imported into Postman to define an environment. Template because there are variables that Terraform replaces as part of the apply. Provided by the F5 lab. Not used in the demo.

- `start.sh`: The starting point to define many environment variables needed to execute the Terraform and Ansible automation. Should be replace by CI/CD pipeline variables. Provided by the F5 lab but customized extensively for the demo.

- `vars.sh`: Terraform variable file that defines variables that are used by main.tf. Defines the format of the variables and default values. Should be overriden by environment variables (shell or CI/CD). Provided by F5 lab, customzied for the demo.

***

## The Companion Project - Juice Shop AWAF (formrerly ASM) Remote Policy

In order to make things clearly separated between this demo of how automation tools can be used to build out F5 BigIP devices in AWS, a second project was created that demonstrate how AWAF policy can be stored remotely from the BigIP and updated via GitLab CI/CD.  That project can be found here:

https://gitlab.com/harmonjeff/juice-shop-asm-waf

The AS3 Juice Shop declarative application defined in the juice-shop-template.json of this project includes a reference to the companion project above.

***

## AWS CLI With MFA Protected User Account
To properly use the AWS CLI with a user account proteced by MFA (security requirement for human users), there are a few special things to understand.

1. In order to make AWS CLI commands you will need to generate a static access key for your IAM user (web console > IAM > Users > {your user} > Security credentials). Put the ACCESS_KEY_ID and the SECRET_ACCESS_KEY in the [~/.aws/credentials](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html) file, which you can have the AWS CLI do for you with this command:

        aws configure

2. This static access key is good until you revoke it, so be careful with it. However, since your user account is protected by MFA you won't actually be authorized to make most AWS CLI commands using that static access key.  You have to obtain temporary credentials using your MFA token:

        aws sts get-session-token --output json --serial-number arn:aws:iam::{aws_account_id}:mfa/{aws_iam_user_id} --token-code {mfa_token}

    If the MFA token was validated you will get a JSON response that looks like this:

        {
            "Credentials": {
                "AccessKeyId": "{temp_access_key_id}",
                "SecretAccessKey": "{teamp_secret_access_key}",
                "SessionToken": "{temp_session_token},
            "Expiration": "{temp_creds_expiration_date}"
            }
        }

    Notice that the response includes a new `AccessKeyId` and `SecretAccessKey` entirely different from the static access key generated in Step 1. In order to make AWS CLI commands with your user protected by MFA you have to override the static access key stored in your ~/.aws/credentials file.  You can specify a new profile in that credentials file or you can define `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_SESSION_TOKEN` environment variables using the values from the JSON above.

    Also notice that these credentials have an expiration date so that you know when it is you will need to obtain new temporary credentials via the same method.

3. Following Steps 1 and 2 you should now be able to make AWS CLI commands that don't work unless ou have valid temporary credentials.  You can test this with a command like:

        aws ec2 describe-instances

    If your temporary credentials have been provided properly (~/.aws/credentials or environment vars) you should be given a listing of instances running in our AWS account.  If you haven't provided correct temporary credentials to the AWS CLI you will get a message like:

        An error occurred (UnauthorizedOperation) when calling the DescribeInstances operation: You are not authorized to perform this operation.

4. This project uses Terraform to build an VPC in an AWS account and create two F5 instances. This project also provides a Postman collection you can use to make REST calls to the F5s that are generated. All of these things require those same temporary credentials.  Though not the only way to provide those temporary credentials, terraform envrionment variables (environment variables prefixed with `TF_VAR_`) are used to specify those credentials for terraform.

5. To aide you in obtaining your temporary credentials and setting the assumed environment variables for this project, you can use `scripts/get-aws-console-credentials.sh`.  The script requires a parameter of your MFA token and setting the `AWS_ACCOUNT_ID` and `AWS_IAM_USER` environment variables specific to you.

    **MAC NOTE: If you are using a Mac the environment variables set in scripts don't survive beyond the execution of that script (scripts are actually run in sub-processes that can't effect the parent). The `get-aws-console-credentials.sh` script generates a .aws-creds file that you will have to source after executing to have the environment variables set for your terminal session.


