#!/bin/bash

if [ -z $AWS_ACCESS_KEY_ID ]
then
    echo "You must set the AWS_ACCESS_KEY_ID environment variable to your AWS account"
    return
fi

if [ -z $AWS_SECRET_ACCESS_KEY ]
then
    echo "You must set the AWS_SECRET_ACCESS_KEY environment variable to your AWS account"
    return
fi

if [ -z $AWS_ACCOUNT_ID ]
then
    echo "You must set the AWS_ACCOUNT_ID environment variable to your AWS account"
    return
fi

if [ -z $AWS_IAM_USER ]
then
    echo "You must set the AWS_IAM_USER environment variable to your AWS account"
    return
fi


if [ -z $emailid ] 
then
    echo "You must set the 'emailid' environment variable to your EWS email address"
    return
fi

if [ -z $emailidsan ] 
then
    echo "You must set the 'emailidsan' environment variable to your EWS email address all lower and with no puctuation/sybmols"
    return
fi

if [ -z $bigip_admin_password ]
then
    echo "You must set the 'bigip_admin_password' environment variable"
    return
fi

if [ -z $restricted_ip_address ]
then
    echo "You must set the 'restricted_ip_address' environment variable to an IP address"
    return
fi

if [ -z $private_ca_password ]
then
    echo "You  must set the 'private_ca_password' enviroment variable to the same secret used to generate private certs"
    return
fi

export TF_VAR_aws_keypair="MyKeyPair-$emailid"
export TF_VAR_emailid=$emailid
export TF_VAR_emailidsan=$emailidsan
export TF_VAR_bigip_admin_password=$bigip_admin_password
export TF_VAR_restricted_ip_address=$restricted_ip_address
export TF_VAR_restrictedSrcAddress="[\"$restricted_ip_address/32\",\"10.2.0.0/16\"]"
export PATH="./scripts:$PATH"
chmod +x ./scripts/*
alias bigip1='ssh -i ./'$aws_keypair'@earlywarning.com.pem admin@$(terraform output Bigip1ManagementEipAddress)'
alias bigip2='ssh -i ./'$aws_keypair'@earlywarning.com.pem admin@$(terraform output Bigip2ManagementEipAddress)'
