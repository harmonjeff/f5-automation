terraform {
required_version = "~> 0.12"
required_providers {
template = "~> 2.1"
local = "~> 1.2"
}
}

/*
  aws ec2 describe-images --region us-west-2 --filters "Name=name,Values=*BIGIP-16.*PAYG-Adv WAF*25Mbps*" | jq '.Images[] |.ImageId, .Name'
  aws ec2 describe-images --region us-west-2 --filters "Name=name,Values=*BIGIP-16.*25Mbps*"
*/

data "aws_ami" "f5" {
  most_recent = true

  filter {
    name   = "name"
    # Use the aws cli command above to find the most recent version
    # This version is most current as of 9/27/2021
    values = ["*BIGIP-16.1.0-0.0.19*3c272b55-0405-4478-a772-d0402ccf13f9*"]
    # values = ["*BIGIP-16.0.1.1-0.0.6*3c272b55-0405-4478-a772-d0402ccf13f9*"]
    # values = ["*BIGIP-16.0.1.1-0.0.6*3e567b08-20a9-444f-a72a-7e8da3c2cbdf*"]
    # values = ["*BIGIP-15.1.2.1-0.0.10*3e567b08-20a9-444f-a72a-7e8da3c2cbdf*"]
    # values = ["*BIGIP-15.1.2-0.0.9*3196aead-f3ab-4f8e-b6a1-0955337224c5*"]
    # values = ["*BIGIP-15.1.0.4-0.0.6*3196aead-f3ab-4f8e-b6a1-0955337224c5*"]
    # values = ["*BIGIP-15.1.0.4-0.0.6*3e567b08-20a9-444f-a72a-7e8da3c2cbdf*"]
    # values = ["*BIGIP-15.0.1-0.0.11*3196aead-f3ab-4f8e-b6a1-0955337224c5*"]
    # values = ["*BIGIP-14.1.2.2-0.0.4*3196aead-f3ab-4f8e-b6a1-0955337224c5*"]
  }
  
  owners = ["aws-marketplace"]
}

data "template_file" "cloud_init" {
  template = file("./cloud-init-bigip-template.yml")
  vars = { 
      restricted_ip_address = var.restricted_ip_address
      restricted_vpc_cidr = var.restrictedSrcAddressVPC[0]
  }
}

resource "local_file" "cloud_init_rendered" {
  content = data.template_file.cloud_init.rendered
  filename = "generated/cloud-init-bigip.yml"
}

# BIGIP 1

resource "aws_instance" "bigip1" {
  depends_on = [
    local_file.cloud_init_rendered
  ]
  ami                          = data.aws_ami.f5.id
  instance_type                = "m5.xlarge"
  key_name                     = var.aws_keypair
  subnet_id                    = aws_subnet.f5-management-a.id
  vpc_security_group_ids       = [aws_security_group.f5_management.id]
  iam_instance_profile         = aws_iam_instance_profile.f5_cloud_failover_profile.name

  # With this set to false, terraform thinks the EC2 instance has to be rebuilt on a second apply due to EIP being attached
  # The public EIP is needed for the purposes of this demo.  It is restricted from public Internet access
  # by both a security group and firewall restrictions on the BigIP management interface (see do-device-sync-template.json)
  # If connectivity to the management interface can be done without using a public IP address and the BigIP can
  # be given Internet access (NAT GW) then this can probably be made false
  associate_public_ip_address  =  true

  # TODO: Use Cloud Init via user_data here to pull the password for admin and root from secrets management via IMDSv2
  # https://clouddocs.f5.com/cloud/public/v1/shared/cloudinit.html
  # May want to have the BigIP reach out to pull and install the iLX extensions this way too

  # Sets the default shell to be bash so that ansible will function correctly
  user_data = data.template_file.cloud_init.rendered

  # TODO: Once Cloud Init is used to set admin password, change F5 instances to be in private subnet and get Internet
  # access through NAT gateway

  tags = {
    Name   = "f5-auto-BIP1: 2az-2nic-PAYG"
    hostname = "bigip1.ewsseclab.dev"
    owner = "${var.emailid}"
  }
}

# The f5_cloud_failover tags are required for the F5 cloud failover extension to auto update the public EIP to the active BigIP
# https://clouddocs.f5.com/products/extensions/f5-cloud-failover/latest/userguide/aws.html#define-the-storage-account-in-aws
# All apps defined on the BigIP will failover so the tags should not be app specific, though they can be any value so long as they match
resource "aws_network_interface" "bigip1_traffic" {
  subnet_id       = aws_subnet.public-a.id
  private_ips     = var.bigip1_private_ip
  security_groups = [aws_security_group.f5_data.id]

  attachment {
    instance     = aws_instance.bigip1.id
    device_index = 1
  }
  tags = {
    Name = "f5-auto-bigip1-traffic"
    f5_cloud_failover_nic_map = "external"
    f5_cloud_failover_label = var.cloud_failover_label
  }
}

# BIGIP 2
resource "aws_instance" "bigip2" {
  depends_on = [
    local_file.cloud_init_rendered
  ]
  ami                         = data.aws_ami.f5.id
  instance_type               = "m5.xlarge"
  key_name                    = var.aws_keypair
  subnet_id                   = aws_subnet.f5-management-b.id
  vpc_security_group_ids      = [aws_security_group.f5_management.id]
  iam_instance_profile        = aws_iam_instance_profile.f5_cloud_failover_profile.name

  associate_public_ip_address  =  true

  # Sets the default shell to be bash so that ansible will function correctly
  user_data = data.template_file.cloud_init.rendered

  tags = {
    Name   = "f5-auto-BIP2: 2az-2nic-PAYG"
    hostname = "bigip2.ewsseclab.dev"
    owner = "${var.emailid}"
  }
}

resource "aws_network_interface" "bigip2_traffic" {
  subnet_id       = aws_subnet.public-b.id
  private_ips     = var.bigip2_private_ip
  security_groups = [aws_security_group.f5_data.id]

  attachment {
    instance     = aws_instance.bigip2.id
    device_index = 1
  }

  tags = {
    Name = "f5-auto-bigip2-traffic"
    f5_cloud_failover_nic_map = "external"
    f5_cloud_failover_label = var.cloud_failover_label
  }
}

# ELASTIC (PUBLIC) IP ADDRESS ASSIGNMENTS FOR VIRTUAL SERVERS
# Next 2 EIPs required for F5 BigIP Cloud Failover
# https://clouddocs.f5.com/cloud/public/v1/aws/AWS_ha.html#task-list-create-ha-interfaces-for-both-big-ip-instances-a-and-b
resource "aws_eip" "bigip1_traffic" {
  vpc                       = true
  network_interface         = aws_network_interface.bigip1_traffic.id
  associate_with_private_ip = var.bigip1_private_ip[0]
  tags = {
    Name = "f5-auto-bigip1-traffic"
  }
}
resource "aws_eip" "bigip2_traffic" {
  vpc                       = true
  network_interface         = aws_network_interface.bigip2_traffic.id
  associate_with_private_ip = var.bigip2_private_ip[0]
  tags = {
    Name = "f5-auto-bigip2-traffic"
  }
}

# In order for cloud-failover to work the EIP tag has to be updated with the IP addresses configured in
# in the terraform vars files.
resource "aws_ec2_tag" "juice_shop_eip" {
  resource_id = var.juice_shop_eip_id
  key = "f5_cloud_failover_vips"
  value = "${var.bigip1_private_ip[1]},${var.bigip2_private_ip[1]}"
}

resource "aws_ec2_tag" "juice_shop_eip_cloud_failover_label" {
  resource_id = var.juice_shop_eip_id
  key = "f5_cloud_failover_label"
  value = "ewsseclabdemo"
}

# Assign pre-existing EIP to the public management interface for convenience of loading BigIP UI in a browser for this demo
# NOTE: It would be better to have the BigIP Management interface not exposed via public IP, if this is done in a real
# deployment take great care in restricting access to IPs that should be able to get to the admin interfaces using
# both AWS SecurityGroups and the management firewall in the BigIP
resource "aws_eip_association" "eip_assoc_bigip1" {
  network_interface_id = aws_instance.bigip1.primary_network_interface_id
  allocation_id = var.bigip1_eip_id
}

resource "aws_eip_association" "eip_assoc_bigip2" {
  network_interface_id = aws_instance.bigip2.primary_network_interface_id
  allocation_id = var.bigip2_eip_id
}

# Assign pre-existing EIP to the Juice Shop application to help with the demo. This allows for a local hosts
# file to map the static IP to a domain name without DNS.  In all real deployments this IP should end up
# in DNS.
resource "aws_eip_association" "eip_assoc_juice_shop" {
  network_interface_id = aws_network_interface.bigip1_traffic.id
  private_ip_address = var.bigip1_private_ip[1]
  allocation_id = var.juice_shop_eip_id
}

# RENDER TEMPLATE FILE
# This is how the original F5 lab applied networking and created the app in the BigIPs.
# This is not used in the EWS Security demo because all of it has been automated with Ansible
data "template_file" "postman" {
  depends_on = [
    null_resource.gen_certs,
    aws_instance.bigip1,
    aws_instance.bigip2
  ]
  template = file("./postman_template.json")
  vars = { 
      AWS_ACCESS_KEY_ID = var.aws_access_key_id
      AWS_SECRET_ACCESS_KEY = var.aws_secret_access_key
      AWS_SESSION_TOKEN = var.aws_session_token
      BIGIP_ADMIN = var.bigip_admin
      BIGIP_ADMIN_PASSWORD = var.bigip_admin_password
      BIGIP1_MGMT_IP_ADDRESS = var.bigip1_mgmt_ip
      BIGIP2_MGMT_IP_ADDRESS = var.bigip2_mgmt_ip
      BIGIP1_MGMT_PRIVATE_ADDRESS = aws_instance.bigip1.private_ip
      BIGIP2_MGMT_PRIVATE_ADDRESS = aws_instance.bigip2.private_ip
      BIGIP1_TRAFFIC_PRIVATE_ADDRESS = var.bigip1_private_ip[0]
      BIGIP2_TRAFFIC_PRIVATE_ADDRESS = var.bigip2_private_ip[0]
      WEB1_PRIVATE_IP_ADDRESS = aws_instance.example-a.private_ip
      WEB2_PRIVATE_IP_ADDRESS = aws_instance.example-b.private_ip
      BIGIP1_DEFAULT_ROUTE = var.bigip1_default_route
      BIGIP2_DEFAULT_ROUTE = var.bigip2_default_route
      BIGIP1_EXAMPLE01_ADDRESS = var.bigip1_private_ip[1]
      BIGIP1_EXAMPLE02_ADDRESS = var.bigip1_private_ip[2]
      BIGIP1_EXAMPLE03_ADDRESS = var.bigip1_private_ip[3]
      BIGIP1_EXAMPLE04_ADDRESS = var.bigip1_private_ip[4]
      BIGIP2_EXAMPLE01_ADDRESS = var.bigip2_private_ip[1]
      BIGIP2_EXAMPLE02_ADDRESS = var.bigip2_private_ip[2]
      BIGIP2_EXAMPLE03_ADDRESS = var.bigip2_private_ip[3]
      BIGIP2_EXAMPLE04_ADDRESS = var.bigip2_private_ip[4]
      PRIVATE_CA = fileexists("certs/private_ca.cert") ? file("certs/private_ca.cert") : "null"
      JUICESHOP_CERT = fileexists("certs/ewsseclab.dev.cert") ? file("certs/ewsseclab.dev.cert") : "null"
      JUICESHOP_KEY = fileexists("certs/ewsseclab.dev.key") ? file("certs/ewsseclab.dev.key") : "null"
  }
}

resource "local_file" "postman_rendered" {
  depends_on = [
    aws_instance.bigip1,
    aws_instance.bigip2
  ]
  content = data.template_file.postman.rendered
  filename = "postman_rendered.json"
}

# Create yaml file needed for the ansible roles to configure the BigIPs
# Ansible is much better suited to configuring the BigIP than terraform and an inventory file with hosts is needed
data "template_file" "inventory" {
  depends_on = [
    aws_instance.bigip1,
    aws_instance.bigip2
  ]
  template = file("./inventory_template.yml")
  vars = {
    bigip1 = var.bigip1_mgmt_ip
    bigip2 = var.bigip2_mgmt_ip
    bigip1_instanceid = aws_instance.bigip1.id
    bigip2_instanceid = aws_instance.bigip2.id
    bigip1_hostname = "bigip1.ewsseclab.dev"
    bigip2_hostname = "bigip2.ewsseclab.dev"
    bigip1_public_management_ip = var.bigip1_mgmt_ip
    bigip2_public_management_ip = var.bigip2_mgmt_ip
    bigip1_private_management_ip = aws_instance.bigip1.private_ip
    bigip2_private_management_ip = aws_instance.bigip2.private_ip
    # Special DNS server address that always works no matter the subnet for AWS EC2 instances
    dns_server = "169.254.169.253"
    search_domain = "ewsseclab.dev"
    bigip1_traffic_self_ip = "${var.bigip1_private_ip[0]}"
    bigip2_traffic_self_ip = "${var.bigip2_private_ip[0]}"
    bigip1_external_self_ip = "${var.bigip1_private_ip[0]}"
    bigip2_external_self_ip = "${var.bigip2_private_ip[0]}"
    bigip1_external_self_ip_netmask = "255.255.255.0"
    bigip2_external_self_ip_netmask = "255.255.255.0"
    bigip1_default_route = var.bigip1_default_route
    bigip2_default_route = var.bigip2_default_route
    bigip1_remote_management_ip = aws_instance.bigip2.private_ip
    bigip2_remote_management_ip = aws_instance.bigip1.private_ip
    juiceshop_a_ip = var.bigip1_private_ip[1]
    juiceshop_b_ip = var.bigip2_private_ip[1]
  }
}

resource "local_file" "inventory_rendered" {
  depends_on = [
    aws_instance.bigip1,
    aws_instance.bigip2
  ]
  content = data.template_file.inventory.rendered
  filename = "ansible/inventory.yml"  
}

# Substitute the IP addresses in the terraform variables into the Juice Shop AS3 declaration JSON
# This is done in Ansible now.
# data "template_file" "juice_json" {
#   depends_on = [
#     aws_instance.bigip1,
#     aws_instance.bigip2
#   ]
#   template = file("./ansible/juice-shop-template.json")
#   vars = { 
#       bigip1_juiceshop_address = var.bigip1_private_ip[1]
#       bigip2_juiceshop_address = var.bigip2_private_ip[1]
#       juiceshop_cert = fileexists("certs/ewsseclab.dev.cert") ? file("certs/ewsseclab.dev.cert") : "null"
#       juiceshop_key = fileexists("certs/ewsseclab.dev.key") ? file("certs/ewsseclab.dev.key") : "null"
#       private_ca = fileexists("certs/private_ca.cert") ? file("certs/private_ca.cert") : "null"
#       restricted_ip_address = var.restricted_ip_address
#   }
# }

# Done by Ansible now
# resource "local_file" "juice_json_rendered" {
#   depends_on = [
#     null_resource.gen_certs,
#     aws_instance.bigip1,
#     aws_instance.bigip2
#   ]
#   content = data.template_file.juice_json.rendered
#   filename = "ansible/juice-shop.json"  
# }


resource "aws_s3_bucket" "f5-public-cloud-failover" {
  bucket = "f5-public-cloud-failover-${uuid()}"
  # Terraform destroy will fail for the bucket not being empty (after being used for BigIP cloud failover) if this is not set
  force_destroy = true
  tags = {
    Name = "f5-auto-cloud-failover"
    f5_cloud_failover_label = var.cloud_failover_label
  }
  lifecycle {
    ignore_changes = [
      bucket
    ]
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "f5-public-cloud-failover-sse" {
  bucket = aws_s3_bucket.f5-public-cloud-failover.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm     = "AES256"
    }
  }
}

resource "aws_s3_bucket_policy" "f5-public-cloud-failover-policy" {
  bucket = aws_s3_bucket.f5-public-cloud-failover.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression's result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Id      = "F5CLOUDFAILOVER"
    Statement = [
      {
        Sid       = "AllowF5CloudFailoverRole"
        Action    = "s3:*"
        Effect    = "Allow"
        Resource  = [
          aws_s3_bucket.f5-public-cloud-failover.arn,
          "${aws_s3_bucket.f5-public-cloud-failover.arn}/*"
        ],
        Principal = {
          "AWS"   = [ 
            aws_iam_role.f5-cloud-failover-role.arn,
            "arn:aws:iam::164757971814:user/Jeff.Harmon@earlywarning.com" 
          ]
        }
      }
    ]
  })
}

# BigIP Device Sync didn't work until their hostnames (bigip1.ewsseclab.dev, bigip2.ewsseclab.dev) were resolveable
resource "aws_route53_zone" "private" {
  depends_on = [
    aws_instance.bigip1,
    aws_instance.bigip2
  ]
  name = "ewsseclab.dev"

  vpc {
    vpc_id = aws_vpc.f5-automation-vpc.id
    vpc_region = var.aws_region
  }

}

resource "aws_route53_record" "bigip1" {
  zone_id = aws_route53_zone.private.zone_id
  name    = "bigip1.ewsseclab.dev"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.bigip1.private_ip]
}

resource "aws_route53_record" "bigip2" {
  zone_id = aws_route53_zone.private.zone_id
  name    = "bigip2.ewsseclab.dev"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.bigip2.private_ip]
}

# ALB in front of the F5 that exposes Juice Shop to the Internet
# This is a demo of how an ALB can be created, but failover when BigIPs change is quite slow
# Much better to use the Cloud Failover extensions and an EIP that points to the primary BigIP
# resource "aws_lb" "juice" {
#   name               = "f5-auto-alb"
#   internal           = false
#   load_balancer_type = "application"
#   security_groups    = [aws_security_group.elb.id]
#   subnets            = [aws_subnet.public-a.id,aws_subnet.public-b.id]

#   enable_deletion_protection = false

#   # This should be turned on in a real example, but saving AWS resources in ewsseclab
#   # access_logs {
#   #   prefix  = "f5-auto-alb"
#   #   enabled = false
#   # }

#   tags = {
#     Environment = "ewsseclab"
#   }
# }

# resource "aws_lb_listener" "juice" {
#   load_balancer_arn = aws_lb.juice.arn
#   port              = "81"
#   protocol          = "HTTP"
#   # SSL should be used in a real implementation of ALB
#   # ssl_policy        = "ELBSecurityPolicy-2016-08"
#   # certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

#   default_action {
#     type             = "forward"
#     target_group_arn = aws_lb_target_group.juice.arn
#   }
# }

# resource "aws_lb_target_group" "juice" {
#   name        = "f5-auto-juice-tg"
#   port        = 80
#   protocol    = "HTTP"
#   target_type = "ip"
#   vpc_id      = aws_vpc.f5-automation-vpc.id
# }

# resource "aws_lb_target_group_attachment" "juice-a" {
#   target_group_arn = aws_lb_target_group.juice.arn
#   target_id        = var.bigip1_private_ip[1]
#   port             = 80
# }

# resource "aws_lb_target_group_attachment" "juice-b" {
#   target_group_arn = aws_lb_target_group.juice.arn
#   target_id        = var.bigip2_private_ip[1]
#   port             = 80
# }


resource "null_resource" "gen_certs" {
  # Took a lot of trial and error to get this functioning
  # AS3 is very picky about how server certificates are defined. Be sure to check
  # out how this script puts the certificates in a format that works, including the
  # removal of the last "=" on the base64 encoding of the passphrase
  provisioner "local-exec" {
    command = "create-private-ca-certs-password.sh"
  }
}

# Clean up the "generated" folder that has temporary Ansible state files between runs
resource "null_resource" "clean_generated" {
  provisioner "local-exec" {
    command = "rm generated/*"
    when = destroy
  }

}

# Use Ansible to install the F5 extensions that are needed for automation
# This is one way to do this, which doesn't require the BigIP to have Internet access
# If cloud failover is going to be used, an external EIP is required anyway, so may be better to do this via cloud-init
resource "null_resource" "install_f5_extensions" {
  depends_on = [
    aws_instance.bigip1,
    aws_instance.bigip2,
    aws_eip_association.eip_assoc_bigip1,
    aws_eip_association.eip_assoc_bigip2,
    aws_eip.bigip1_traffic,
    aws_eip.bigip2_traffic
  ]
  provisioner "local-exec" {
    command = "ansible-playbook -i ansible/inventory.yml --private-key MyKeyPair-${var.emailid}.pem -u admin ansible/Install-F5-Extensions.yml"
  }
}

# Use Ansible to do the following via F5 Decalartive Onboarding:
# - provision LTM and ASM modules
# - apply basic networking settings (DNS, NTP, self IP, default route, etc)
# - establish device trust
# - define device sync for active/standby
resource "null_resource" "apply_networking_to_bigips" {
  depends_on = [
    null_resource.install_f5_extensions,
    aws_instance.bigip1,
    aws_instance.bigip2,
    aws_eip_association.eip_assoc_bigip1,
    aws_eip_association.eip_assoc_bigip2,
    aws_eip.bigip1_traffic,
    aws_eip.bigip2_traffic
  ]
  provisioner "local-exec" {
    command = "ansible-playbook -i ansible/inventory.yml --private-key MyKeyPair-${var.emailid}.pem -u admin ansible/Apply-Networking.yml"
  }
}

data "template_file" "cloud_failover" {
  template = file("./ansible/cloud-failover-template.json")
  vars = { 
    cloud_failover_label = var.cloud_failover_label
  }
}

resource "local_file" "cloud_failover_rendered" {
  content = data.template_file.cloud_failover.rendered
  filename = "ansible/cloud-failover.json"  
}

resource "null_resource" "define_cloud_failover" {
  depends_on = [
    local_file.cloud_failover_rendered,
    null_resource.apply_networking_to_bigips,
    aws_instance.bigip1,
    aws_instance.bigip2,
    aws_eip_association.eip_assoc_bigip1,
    aws_eip_association.eip_assoc_bigip2,
    aws_eip.bigip1_traffic,
    aws_eip.bigip2_traffic
  ]
  provisioner "local-exec" {
    command = "ansible-playbook -i ansible/inventory.yml --private-key MyKeyPair-${var.emailid}.pem -u admin ansible/Define-Cloud-Failover.yml"
  }
}

# Use Ansible to use F5 DO to apply custom server certificates to each BigIP
# Updating a device certificate reboots the BigIP, and while it didn't need to be a last step, if done along with a lot of other DO configuration
# changes some of the changes fail. To be safe, updating the device certificate should be the only change defined in the DO JSON
resource "null_resource" "device_certificates" {
  depends_on = [
    null_resource.define_cloud_failover,
    aws_instance.bigip1,
    aws_instance.bigip2,
    aws_eip_association.eip_assoc_bigip1,
    aws_eip_association.eip_assoc_bigip2,
    aws_eip.bigip1_traffic,
    aws_eip.bigip2_traffic,
  ]
  provisioner "local-exec" {
    command = "ansible-playbook -i ansible/inventory.yml --private-key MyKeyPair-${var.emailid}.pem -u admin ansible/Apply-Device-Certificate.yml"
  }
}

# Use Ansible to do the folowing via F5 Automation Services (AS3): 
# - create a "juice-shop-pool" pool that auto-adds nodes for all AWS instances in the private subnets tagged with a "findme", "web" name value pair
# - create a "clientssl_secure_juiceshop" ssl profile that uses the self-signed certificates the BigIP generated (real certificates can and should be used)
# - create an "http2_juiceshop" HTTP service
# - creates 2 redirect virtual servers that will redirect from 80 to 443
# - creates 2 virtual servers for the juice shop pool, one for each us-west-2 regiong (a and b) where only one is actually active at a time
resource "null_resource" "create_juice_shop" {
  depends_on = [
    null_resource.device_certificates,
    aws_instance.bigip1,
    aws_instance.bigip2,
    aws_eip_association.eip_assoc_bigip1,
    aws_eip_association.eip_assoc_bigip2,
    aws_eip.bigip1_traffic,
    aws_eip.bigip2_traffic,
  ]
  provisioner "local-exec" {
    command = "ansible-playbook -i ansible/inventory.yml --private-key MyKeyPair-${var.emailid}.pem -u admin ansible/Create-Juice-Shop.yml"
  }
}