# Defines the iam role assigned to the EC2 instances running the BigIPs
# NOTE: This role is OVERLY PERMISSIVE to facilitate testing and createing the demo
# THIS ROLE MUST NOT BE USED IN REAL DEPLOYMENTS.
# https://clouddocs.f5.com/products/extensions/f5-cloud-failover/latest/userguide/aws.html#create-and-assign-an-iam-role
resource "aws_iam_role" "f5-cloud-failover-role" {
  name = "f5-automation-cloud-failover-role"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy" "sts_assume_role" {
  name        = "f5-automation_sts_assume_role"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Resource": "*"
        }
    ]
}

EOF
}

resource "aws_iam_policy" "ec2_all" {
  name        = "f5-automation_ec2_all"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "ec2:*",
            "Resource": "*"
        }
    ]
}

EOF
}

resource "aws_iam_policy" "s3_all" {
  name        = "f5-automation_s3_all"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "*"
        }
    ]
}

EOF
}

resource "aws_iam_role_policy_attachment" "attach1_f5_automation" {
  role       = aws_iam_role.f5-cloud-failover-role.name
  policy_arn = aws_iam_policy.sts_assume_role.arn
}
resource "aws_iam_role_policy_attachment" "attach2_f5_automation" {
  role       = aws_iam_role.f5-cloud-failover-role.name
  policy_arn = aws_iam_policy.ec2_all.arn
}
resource "aws_iam_role_policy_attachment" "attach3_f5_automation" {
  role       = aws_iam_role.f5-cloud-failover-role.name
  policy_arn = aws_iam_policy.s3_all.arn
}

resource "aws_iam_instance_profile" "f5_cloud_failover_profile" {
  name  = "f5_cloud_failover_profile"
  role = aws_iam_role.f5-cloud-failover-role.name
}
