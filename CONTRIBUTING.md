This repository is meant to serve as an example of how F5 automation toolchain pieces can be used to automate the creation, configuration, updating, and destruction of BigIP devices in AWS.

This example does <b>NOT</b> represent a fully hardened implementation of BigIP devices, AWAF policy, DOS Profile, or Bot protection. It should not be directly used to create a production environment of any kind.

If you are interested in helping keep the examples and or documentation updated, please contact Jeff Harmon at harmon dot jeff at gmail dot com or <a href="https://gitlab.com/harmonjeff/f5-automation/-/project_members">request to be added as a team member</a> through GitLab.
